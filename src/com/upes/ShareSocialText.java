package com.upes;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.upes.object.AppsConstant;
import com.upes.object.MyPreferences;

public class ShareSocialText {

	private Context context;
	private String username = "";
	private MyPreferences myPreferences;


	public ShareSocialText(Context context) {
		this.context = context;
		myPreferences = new MyPreferences(context);
	}


	public Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			if(msg.equals("success")){
				Log.e("Share Social : ", "Alert Posted in Social Sites");
			}else if(msg.equals("relogin")){
				Log.e("Share Social : ", "Relogin require in facebook");
				Toast.makeText(context, "Please Re-login to facebook from setting menu !", Toast.LENGTH_SHORT).show();
			}else if(msg.equals("failed")){
				Log.e("Share Social : ", "Failed to post alert in Social Sites");
			}
		}
	};



	/**
	 * This Method Share the post in facebook
	 */
	public void shareOnFacebookWall(){
		Log.e("Sahre via facebook", "Sharing via facebook wall");
		new Thread(new Runnable() {
			public void run() {
				try {
					String desc = " ";

					String mesg = " ";

					AppsConstant.facebook = new Facebook(AppsConstant.FACEBOOK_APP_ID);
					String st = myPreferences.getFacebookToken();
					Bundle parameters = new Bundle();
					parameters.putString("message",mesg);
					parameters.putString("description", ""+desc);
					parameters.putString("picture", "");
					parameters.putString("link", "https://play.google.com/store/apps/details?id=com.upes&feature=search_result#?t=W251bGwsMSwyLDEsImNvbS51cGVzIl0");
					parameters.putString("caption", "Try the UPES App!");
					parameters.putString("name", " UPES App");

					if (st.length() > 0) {
						parameters.putString("access_token", "" + st);
					}
					AppsConstant.facebook.request("me");
					String response = AppsConstant.facebook.request("me/feed", parameters, "POST");

					Log.d("Tests--->*************", "got response: " + response);

					Message msg=new Message();
					if (response == null
							|| response.equals("")
							|| response.equals("false")
							|| response.equalsIgnoreCase("{\"error\":{\"message\":\"An active access token must be used to query information about the current user.\",\"type\":\"OAuthException\",\"code\":2500}}")) {
						msg.obj ="relogin";
					}else{
						msg.obj="success";
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					e.printStackTrace();
					Message msg=new Message();
					msg.obj="failed";
					handler.sendMessage(msg);
				}
			}
		}).start();
	}

}
