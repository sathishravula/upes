package com.upes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BaseActivity extends Activity implements OnClickListener{
	
	ImageView imvPhone;
	Button btnHeader;
	TextView tvHeader;
	LinearLayout llMain;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_PROGRESS);
	    this.setProgressBarVisibility(true);
		setContentView(R.layout.base);
		
		llMain = (LinearLayout)findViewById(R.id.ll_main);
		btnHeader = (Button)findViewById(R.id.btn_header);
		imvPhone = (ImageView)findViewById(R.id.imv_phone);
		tvHeader = (TextView)findViewById(R.id.tv_header);
		
		btnHeader.setOnClickListener(this);
		imvPhone.setOnClickListener(this);
}

	@Override
	public void onClick(View v) 
	{
		if(v == btnHeader)
		{
			Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else if(v == imvPhone)
		{
			String number = "tel: +18001028737";
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
            startActivity(callIntent);
		}
		
	}
	
	public void showMessageAlert(String strMSG)
	{
		AlertDialog.Builder alrt= new AlertDialog.Builder(BaseActivity.this);
		alrt.setMessage(strMSG);
		alrt.setCancelable(false);
		
		alrt.setNeutralButton("OK", new  DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.dismiss();
			}
		});
		alrt.show();
	}
	
	public static boolean isNetworkAvailable(Context ctx)
	{
		ConnectivityManager cm =
		        (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

	    //NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		    NetworkInfo i = cm.getActiveNetworkInfo();
		    if (i == null)
		      return false;
		    if (!i.isConnected())
		      return false;
		    if (!i.isAvailable())
		      return false;
		    return true;

	}
}