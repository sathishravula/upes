package com.upes;

import java.net.MalformedURLException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.connection.HttpConnection;
import com.upes.object.AppsConstant;

public class Social extends BaseActivity implements OnClickListener{
	
	ListView lvSocial;
	ImageView imvFb, imvTw;
	WebView webFb;
	ListSocilaAdapter listSocilaAdapter;
	LayoutInflater inflater;
	LinearLayout llMainContent;
	
	ProgressDialog pd;
	ProgressBar pg;
	boolean flag = true;
	HttpConnection http;
	String url = "https://www.facebook.com/UniversityofPetroleumandEnergyStudies";
	 
	//url = "http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=UPESDehradun";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		setUpUI();
		
		//parseUrl(url);
		switchFb();
		
		lvSocial.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Social.this, SocialDesc.class);
				intent.putExtra("link", AppsConstant.vec_socialInfo.get(arg2).link);
				startActivity(intent);
				
//				String strURL = AppsConstant.vec_socialInfo.get(arg2).link;
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
//				startActivity(intent);
				
			}
		});
		
		
		webFb.setWebViewClient(new WebViewClient()       
	    {
	         @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) 
	        {
	            return false;
	        }
	    });
		final Activity activity = this;
		webFb.setWebChromeClient(new WebChromeClient()
		{
		    public void onProgressChanged(WebView view, int progress) 
		    {
		        activity.setProgress(progress * 100);
		        if(progress==100)
		        	pg.setVisibility(View.GONE);
		    }
		    
		});
		
	}
	
	
	private void setUpUI() 
	{
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.social, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Social");
		
        pg= (ProgressBar)llMainContent.findViewById(R.id.pg1);
        pg.setVisibility(View.GONE);
        lvSocial = (ListView)llMainContent.findViewById(R.id.lv_social);
        webFb = (WebView)llMainContent.findViewById(R.id.web_fb);
        imvFb = (ImageView)llMainContent.findViewById(R.id.imv_fb);
		imvTw = (ImageView)llMainContent.findViewById(R.id.imv_tw);
		
		imvFb.setOnClickListener(this);
		imvTw.setOnClickListener(this);
		
	}
	
	private void parseUrl(final String strURL) 
	{
		pd=new ProgressDialog(Social.this);
		pd.setMessage("Loading...");
		pd.setCancelable(false);
	     
		pd.show();
		Thread th=new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				http= new HttpConnection();
				try
				{
					http.getSocial(strURL);
				} 
				catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}	
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						pd.dismiss();
						if(AppsConstant.vec_socialInfo!=null && AppsConstant.vec_socialInfo.size()>0)
						{
							listSocilaAdapter=new ListSocilaAdapter();
							lvSocial.setAdapter(listSocilaAdapter);
						}
					}
				});
				
			}
		});
      
      if(isNetworkAvailable(Social.this))
      {
    	  th.start();
      }
      else
      {
    	  pd.dismiss();
    	  Toast.makeText(Social.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
      }
	}

	
	class ListSocilaAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AppsConstant.vec_socialInfo.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			convertView = inflater.inflate(R.layout.list_social, null);
			
			TextView tvTitle = (TextView)convertView.findViewById(R.id.tv_title_social);
			TextView tvSocialDesc = (TextView)convertView.findViewById(R.id.tv_desc_social);
			
			tvTitle.setText("@UPESDehradun");
			String description = AppsConstant.vec_socialInfo.get(position).description.replaceAll("\\<.*?>","").trim();
			tvSocialDesc.setText(description);
			
			return convertView;
		}
		
	}
	
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		if(v == btnHeader)
		{
			Intent intent = new Intent(Social.this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else if(v == imvPhone)
		{
			String number = "tel: +18001028737";
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
            startActivity(callIntent);
		}
		else if(v == imvTw)
		{
			switchTwitter();
//			if(flag)
//				switchTwitter();
//			else
//				switchFb();
		}
		else if(v == imvFb)
		{
			switchFb();
//			if(flag)
//			{
//				switchFb();
//				flag = false;
//			}
//			else
//			{
//				switchTwitter();
//				flag = true;
//			}
			
			
		}
		
	}


	private void switchTwitter() 
	{
		imvTw.setImageResource(R.drawable.t_pressed);
		imvFb.setImageResource(R.drawable.f_bg);
		
		webFb.setVisibility(View.GONE);
		lvSocial.setVisibility(View.VISIBLE);
		
		url = "https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=UPESDehradun";
		parseUrl(url);
	}
	
	private void switchFb() 
	{
		imvTw.setImageResource(R.drawable.t_bg);
		imvFb.setImageResource(R.drawable.f_pressed);
		
//		imvTw.setImageResource(R.drawable.f_pressed);
//		imvFb.setImageResource(R.drawable.t_bg);
		url = "https://www.facebook.com/UniversityofPetroleumandEnergyStudies";
		
		pg.setVisibility(View.VISIBLE);
		webFb.setVisibility(View.VISIBLE);
		lvSocial.setVisibility(View.GONE);
		webFb.loadUrl(url);
		
//		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//		startActivity(intent);
		
		//url = "https://www.facebook.com/feeds/page.php?id=247479797001&format=rss20";
		//parseUrl(url);
	}


}
