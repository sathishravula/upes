package com.upes;

import java.io.InputStream;
import java.net.MalformedURLException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.upes.connection.PartnershipParser;
import com.upes.object.AppsConstant;

public class Partnership extends BaseActivity implements OnClickListener{
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	ListView lvPartnerships;
	//ExpandableListView expPartnerships;
	ListAdapter listAdapter;
	//ListPartnershipsAdapter listPartners;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.about_us_detail2, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Partnerships");
        btnHeader.setVisibility(View.GONE);
        
        lvPartnerships = (ListView)llMainContent.findViewById(R.id.lv_aboutus_detail);
//        expPartnerships = (ExpandableListView)llMainContent.findViewById(R.id.exp_partnerships);
//        expPartnerships.setVisibility(View.VISIBLE);
        //lvAboutusDetail.setVisibility(View.GONE);
        
		try
		{
			InputStream inStream = getApplicationContext().getAssets().open("partner.xml"); 
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			
		    PartnershipParser myXMLHandler = new PartnershipParser();
			xr.setContentHandler(myXMLHandler);
			xr.parse(new InputSource(inStream));

		}
			
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
		
		if(AppsConstant.vec_partnershipInfo!=null && AppsConstant.vec_partnershipInfo.size()>0)
		{
			listAdapter=new ListAdapter();
			lvPartnerships.setAdapter(listAdapter);
			
//			listPartners = new ListPartnershipsAdapter();
//			expPartnerships.setAdapter(listPartners);
		}
		
		lvPartnerships.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Partnership.this, Partners.class);
				intent.putExtra("country_pos", arg2);
				startActivity(intent);
				
			}
		});
		
	}
	
	class ListAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AppsConstant.vec_partnershipInfo.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			convertView = inflater.inflate(R.layout.list_items, null);

			TextView tvCountryName= (TextView)convertView.findViewById(R.id.tv_items);
			
			tvCountryName.setText(AppsConstant.vec_partnershipInfo.get(position).country);
			
			return convertView;
		}
		
	}

	
//	private class ListPartnershipsAdapter extends BaseExpandableListAdapter
//	{
//		@Override
//		public Object getChild(int groupPosition, int childPosition) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public long getChildId(int groupPosition, int childPosition) {
//			// TODO Auto-generated method stub
//			return 0;
//		}
//
//		@Override
//		public View getChildView(final int groupPosition, int childPosition,
//				boolean isLastChild, View convertView, ViewGroup parent) {
//			
//			inflater = getLayoutInflater();
//			convertView = (LinearLayout)inflater.inflate(R.layout.list_news, null);
//			
//			TextView tvUniName= (TextView)convertView.findViewById(R.id.tv_title);
//			TextView tvCity = (TextView)convertView.findViewById(R.id.tv_news_desc);
//			TextView tvArea = (TextView)convertView.findViewById(R.id.tv_put_date);
//			
//			tvUniName.setText(AppsConstant.vec_partnershipInfo.get(groupPosition).vec_detail.get(childPosition).university);
//			tvCity.setText(AppsConstant.vec_partnershipInfo.get(groupPosition).vec_detail.get(childPosition).city);
//			tvArea.setText(AppsConstant.vec_partnershipInfo.get(groupPosition).vec_detail.get(childPosition).area);
//			
//			tvUniName.setPadding(10,0,10, 0);
//			tvCity.setPadding(10, 0, 10, 0);
//			tvArea.setPadding(10, 0,10, 0);
//			tvCity.setTextColor(Color.BLACK);
//			tvArea.setTextColor(Color.BLACK);
//			
//			return convertView;
//		}
//
//		@Override
//		public int getChildrenCount(int groupPosition) 
//		{
//			return AppsConstant.vec_partnershipInfo.get(groupPosition).vec_detail.size();
//		}
//
//		@Override
//		public Object getGroup(int groupPosition) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public int getGroupCount() {
//			// TODO Auto-generated method stub
//			return AppsConstant.vec_partnershipInfo.size();
//		}
//
//		@Override
//		public long getGroupId(int groupPosition) {
//			// TODO Auto-generated method stub
//			return 0;
//		}
//
//		@Override
//		public View getGroupView(int groupPosition, boolean isExpanded,
//				View convertView, ViewGroup parent) {
//			
//			expPartnerships.expandGroup(groupPosition);
//			inflater = getLayoutInflater();
//			convertView = (LinearLayout)inflater.inflate(R.layout.list_items, null);
//			
//			TextView tvCountryName= (TextView)convertView.findViewById(R.id.tv_items);
//			
//			tvCountryName.setText(AppsConstant.vec_partnershipInfo.get(groupPosition).country);
//			tvCountryName.setBackgroundColor(Color.parseColor("#b0b0b0"));
//			tvCountryName.setTextColor(Color.WHITE);
//			tvCountryName.setPadding(10, 10, 10, 10);
//			
//			return convertView;
//		}
//
//		@Override
//		public boolean hasStableIds() {
//			// TODO Auto-generated method stub
//			return false;
//		}
//
//		@Override
//		public boolean isChildSelectable(int groupPosition, int childPosition) {
//			// TODO Auto-generated method stub
//			return false;
//		}
//
//	}
//	
}