package com.upes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class NewsDescription extends BaseActivity {
	
	LayoutInflater inflater;
	RelativeLayout llMainContent;
	WebView web;
	ListView lvNews;
	ProgressDialog pd;
	String strURL = "";
	ProgressBar pg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();		
		llMainContent = new RelativeLayout(this);
		llMainContent = (RelativeLayout)inflater.inflate(R.layout.news, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Domain News");
        btnHeader.setVisibility(View.GONE);
        pg= (ProgressBar)llMainContent.findViewById(R.id.pgg);
        pg.setVisibility(View.VISIBLE);
        lvNews = (ListView)llMainContent.findViewById(R.id.lv_news);
		web= (WebView)llMainContent.findViewById(R.id.web_news);
		
		web.setVisibility(View.VISIBLE);
		lvNews.setVisibility(View.GONE);
		
		strURL = getIntent().getExtras().getString("link");
		
		web.setWebViewClient(new WebViewClient()       
	    {
	         @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) 
	        {
	            return false;
	        }
	    });
		
		final Activity activity = this;
		web.setWebChromeClient(new WebChromeClient()
		{
		    public void onProgressChanged(WebView view, int progress) 
		    {
		        activity.setProgress(progress * 100);
		        if(progress==100)
		        	pg.setVisibility(View.GONE);
		    }
		    
		  
		});
		
		
		WebSettings settings = web.getSettings();
		settings.setBuiltInZoomControls(true);
		settings.setUseWideViewPort(true);
		settings.setJavaScriptEnabled(true);
//		settings.setSupportMultipleWindows(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setLoadsImagesAutomatically(true);
		settings.setLightTouchEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setLoadWithOverviewMode(true);
		
		web.loadUrl(strURL);
		
	}
	
}
