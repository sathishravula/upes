package com.upes;

import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class GoogleMapActivity extends MapActivity implements OnClickListener {
  /**
   * Called when the activity is first created.
   */

  LinearLayout zoom;
  MapView mapView;
  MapController mc;
  GeoPoint gp;
  double latitude, longitude;

  String zip, selectedCity;
  ImageView imvHome, imvPhone;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.map);

    latitude = 30.417219;
    longitude = 77.967572;

    mapView = (MapView) findViewById(R.id.mapView);

    imvHome = (ImageView) findViewById(R.id.imv_home2);
    imvPhone = (ImageView) findViewById(R.id.imv_phone2);
    imvHome.setOnClickListener(this);
    imvPhone.setOnClickListener(this);

    LinearLayout zoomLayout = (LinearLayout) findViewById(R.id.zoom);
    View zoomView = mapView.getZoomControls();

    zoomLayout.addView(zoomView,
        new LinearLayout.LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT));
    mapView.displayZoomControls(true);

    mapView.setSatellite(true);
    mc = mapView.getController();

    gp = new GeoPoint((int) (latitude * 1E6), (int) (longitude * 1E6));
    mc.animateTo(gp);
    mc.setZoom(18);
    MapOverlay mapOverlay = new MapOverlay();
    List<Overlay> listOverlay = mapView.getOverlays();
    listOverlay.clear();
    listOverlay.add(mapOverlay);

   // mapView.invalidate();

  }

  @Override
  protected boolean isRouteDisplayed() {
    // TODO Auto-generated method stub
    return false;
  }

  class MapOverlay extends com.google.android.maps.Overlay {
    public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
      super.draw(canvas, mapView, shadow);
      Point screenPoint = new Point();
      mapView.getProjection().toPixels(gp, screenPoint);
      Bitmap btm = BitmapFactory.decodeResource(getResources(), R.drawable.pin);
      canvas.drawBitmap(btm, screenPoint.x, screenPoint.y - 38, null);
      return true;
    }
  }

  @Override
  public void onClick(View v) {
    if (v == imvHome) {
      Intent intent = new Intent(GoogleMapActivity.this, HomeActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
    } else if (v == imvPhone) {
      String number = "tel: +18001028737";
      Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
      startActivity(callIntent);
    }
  }

}

