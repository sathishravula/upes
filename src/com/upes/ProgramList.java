package com.upes;

import java.util.Vector;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.connection.DatabaseHelper;
import com.upes.object.AppsConstant;
import com.upes.object.ProgramInfo;

public class ProgramList extends BaseActivity implements OnClickListener{
	
	ListView lvPrograms;
	ListAdapter listAdapter;
	LayoutInflater inflater;
	LinearLayout llMainContent, llButtons;
	
	int btn = 1;
	String selectedItem = "All";
	
	SQLiteDatabase _database = DatabaseHelper.openDataBase();
	
	String [] Sector;
	//= new String [] {"Electronics", "Information Technology","Infrastructure", "Inter-disciplinary", "Legal Studies", "Logistics & Supply Chain", "Oil & Gas","Power","Transportation" };
	String [] Degree;
	//= new String [] {"Post Graduation","Graduation"};
	String [] Prog;
	//= new String [] {"MBA 2 Year Full Time","M.TECH. 2 Year Full Time","BBA 3 Year Full Time", "B. TECH. 4 Year Full Time", "L.L.B. 5 Year Full Time"};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		setUpUI();
		
		btn = getIntent().getExtras().getInt("prog_btn");
		
		if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
		{
			switch (btn) {
			case 2:
				getSector();
				tvHeader.setText("Sectors");
				listAdapter=new ListAdapter(Sector);
				lvPrograms.setAdapter(listAdapter);
				break;
			case 3:
				getDegree();
				tvHeader.setText("Degrees");
				listAdapter=new ListAdapter(Degree);
				lvPrograms.setAdapter(listAdapter);
				break;
			case 4:
				getProgram();
				tvHeader.setText("Programs");
				listAdapter=new ListAdapter(Prog);
				lvPrograms.setAdapter(listAdapter);
				break;

			}
		}
		else
		{
			Toast.makeText(ProgramList.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
		}
		
	}
	
	private void setUpUI() 
	{
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.programs, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
		btnHeader.setVisibility(View.GONE);
        
        llButtons = (LinearLayout)llMainContent.findViewById(R.id.ll_buttons);
        llButtons.setVisibility(View.GONE);
		
		lvPrograms = (ListView)llMainContent.findViewById(R.id.lv_programs);
		
		
	}
	
	class ListAdapter extends BaseAdapter
	{
		String listProg[];
		public ListAdapter(String list[]) 
		{
			listProg = list;
		}

		@Override
		public int getCount() 
		{
			return listProg.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			convertView=(LinearLayout)(inflater).inflate(R.layout.list_programs, null);
			
			TextView tvProgName = (TextView)convertView.findViewById(R.id.tv_prog_name);
			//ImageView imvProgram = (ImageView)convertView.findViewById(R.id.imv_cap);
			
			//imvProgram.setVisibility(View.GONE);
			tvProgName.setText(listProg[position]);
			
			convertView.setTag(position);
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					int pos = (Integer) v.getTag();
					selectedItem = listProg[pos];
					makeSelectedVector(selectedItem);
					
				}
			});
			
			return convertView;
		}

		protected void makeSelectedVector(String selectedItem) 
		{
			AppsConstant.vec_prog = new Vector<ProgramInfo>();
			if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
    		{
				for(int position=0; position<AppsConstant.vec_programInfo.size(); position++)
				{
					switch (btn) {
//					case 1:
//						//tvProgName.setText(AppsConstant.vec_programInfo.get(position).cName);
//						break;
					case 2:
						if(AppsConstant.vec_programInfo.get(position).sector.equalsIgnoreCase(selectedItem))
						{
							AppsConstant.vec_prog.add(AppsConstant.vec_programInfo.get(position));
						}
						break;
					case 3:
						if(AppsConstant.vec_programInfo.get(position).degree.equalsIgnoreCase(selectedItem))
						{
							AppsConstant.vec_prog.add(AppsConstant.vec_programInfo.get(position));
						}
						break;
					case 4:
						if(AppsConstant.vec_programInfo.get(position).pName.equalsIgnoreCase(selectedItem))
						{
							AppsConstant.vec_prog.add(AppsConstant.vec_programInfo.get(position));
						}
						break;

					}
				}
				
				Intent intent = new Intent(ProgramList.this, ProgramsAll.class);
				AppsConstant.btnAll = false;
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				
    		}
				
		}
		
	}
	
	
	public void getSector() 
	{
		Cursor c;
		String query = "Select distinct sector from course_details order by sector asc";
		c = DatabaseHelper.get(query);
		
		int i = 0;
		
		if(c.moveToFirst())
	    {
			Sector = new String[c.getCount()];
			do
			{
				try 
				{
					Sector[i] = c.getString(0);
					i++;
				} 
				catch (Exception e)
				{
					Log.i("Error in getting data ", e.toString());
				}
					
			} while (c.moveToNext());
	    }
		c.close();

	}
	
	public void getDegree() 
	{
		Cursor c;
		String query = "Select distinct degree from course_details order by degree asc";
		c = DatabaseHelper.get(query);
		
		int i = 0;
		
		if(c.moveToFirst())
	    {
			Degree = new String[c.getCount()];
			do
			{
				try 
				{
					Degree[i] = c.getString(0);
					i++;
				} 
				catch (Exception e)
				{
					Log.i("Error in getting data ", e.toString());
				}
					
			} while (c.moveToNext());
	    }
		c.close();

	}
	
	public void getProgram() 
	{
		Cursor c;
		String query = "Select distinct pName from course_details order by  pName asc";
		c = DatabaseHelper.get(query);
		
		int i = 0;
		if(c.moveToFirst())
	    {
			Prog = new String[c.getCount()];
			do
			{
				try 
				{
					Prog[i] = c.getString(0);
					i++;
				} 
				catch (Exception e)
				{
					Log.i("Error in getting data ", e.toString());
				}
					
			} while (c.moveToNext());
	    }
		c.close();

	}

}
