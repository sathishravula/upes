package com.upes;

import android.R.color;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
 
public class AboutUsDetail extends BaseActivity {
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	ImageView image;
	TextView tvAboutUSDetail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.about_us_detail, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("About Us");
        btnHeader.setVisibility(View.GONE);
        
		image = (ImageView)llMainContent.findViewById(R.id.imv_about_us_detail);
		tvAboutUSDetail = (TextView)llMainContent.findViewById(R.id.tv_about_us_detail);
		
		int i = 1;
		i = getIntent().getExtras().getInt("about");
		switch (i) {

		case 1:
			tvHeader.setText("Chancellor's Message");
			image.setImageResource(R.drawable.sjchopra);
			tvAboutUSDetail.setText(getResources().getString(R.string.chancellor));
			break;
			
		case 2:
			tvHeader.setText("VC's Message");
			image.setImageResource(R.drawable.parag_diwan_vice_chancellor1);
			tvAboutUSDetail.setText(getResources().getString(R.string.vc));
			break;
			
		}
		
		
	}
	
}
