package com.upes;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.connection.DatabaseConnection;
import com.upes.connection.HttpConnection;
import com.upes.object.AppsConstant;
import com.upes.object.ProgramInfo;

public class ProgramsAll extends BaseActivity implements OnClickListener{
	
	ListView lvPrograms;
	ListProgramAdapter listProgramAdapter;
	LayoutInflater inflater;
	LinearLayout llMainContent, llButtons;
	
	ProgressDialog pd;
	HttpConnection http;
	
	Button btnAll, btnSector, btnDegree,btnProg;
	int btn = 1;
	String selectedItem = "All";
	
	DatabaseConnection data = new DatabaseConnection();
//	SQLiteDatabase _database = DatabaseHelper.openDataBase();
	
	public static Vector<ProgramInfo> vec_prog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		setUpUI();
		
		if(AppsConstant.btnAll)
		{
			data.getTimeStamp();
			if(AppsConstant.dates !=null && AppsConstant.dates.length()>0)
			{
				if(!isNetworkAvailable(ProgramsAll.this))
				{
					data.getCourseDetails();
    				if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
	        		{
	        			sort("cName", AppsConstant.vec_programInfo);
	        			listProgramAdapter=new ListProgramAdapter(AppsConstant.vec_programInfo);
	        			lvPrograms.setAdapter(listProgramAdapter);
	        		}
				}
				else
					parseTimeStamp();
			
			}
			else
				parseTimeStamp();
		}
		else
		{
			if(AppsConstant.vec_prog!=null && AppsConstant.vec_prog.size()>0)
    		{
    			listProgramAdapter=new ListProgramAdapter(AppsConstant.vec_prog);
    			lvPrograms.setAdapter(listProgramAdapter);
    		}
    		else
    		{
    			Toast.makeText(ProgramsAll.this, "Please Check Your Network", Toast.LENGTH_LONG).show();
    		}
		}
		
	}
	

	private void setUpUI() 
	{
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.programs, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("");
		
        llButtons = (LinearLayout)llMainContent.findViewById(R.id.ll_buttons);
		btnAll = (Button)llMainContent.findViewById(R.id.btn_prog_all);
		btnSector = (Button)llMainContent.findViewById(R.id.btn_prog_sector);
		btnDegree = (Button)llMainContent.findViewById(R.id.btn_prog_degree);
		btnProg = (Button)llMainContent.findViewById(R.id.btn_prog);
		
		lvPrograms = (ListView)llMainContent.findViewById(R.id.lv_programs);
		
		btnAll.setBackgroundResource(R.drawable.btn1_press);
		btnAll.setOnClickListener(this);
		btnSector.setOnClickListener(this);
		btnDegree.setOnClickListener(this);
		btnProg.setOnClickListener(this);
		
	}
	
	private void parseTimeStamp() 
	{
		pd=new ProgressDialog(ProgramsAll.this);
        pd.setMessage("Loading...");
        pd.show();
        Thread th=new Thread(new Runnable() 
        {
			@Override
			public void run() 
			{
				http= new HttpConnection();
		        try
		        {
					http.getTimeStamp(new URL("http://upes.ac.in/service.asmx/get_date"));
				} 
		        catch (MalformedURLException e)
		        {
					e.printStackTrace();
				}
		        catch (Exception e) 
		        {
					e.printStackTrace();
				}	
		        
		        runOnUiThread(new Runnable() {
		        	
		        	@Override
		        	public void run() {
		        		// TODO Auto-generated method stub
		        		pd.dismiss();
		        		if(AppsConstant.parseDates!=null && AppsConstant.parseDates.length()>0)
		        		{
		        			String ch = AppsConstant.dates;
		        			if(!(AppsConstant.dates.equalsIgnoreCase(AppsConstant.parseDates)))
		        			{
		        				data.deleteTimeStamp();
		        				data.putTimeStamp();
		        				//AppsConstant.dates = AppsConstant.parseDates;
		        				parseProgramData();
		        			}
		        			else
		        			{
		        				data.getCourseDetails();
		        				if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
				        		{
				        			sort("cName", AppsConstant.vec_programInfo);
				        			listProgramAdapter=new ListProgramAdapter(AppsConstant.vec_programInfo);
				        			lvPrograms.setAdapter(listProgramAdapter);
				        		}
		        				else
		        					parseProgramData();

		        			}
		        			
		        		}
		        		else
		        		{
		        			Toast.makeText(ProgramsAll.this, "Please Check Your Network", Toast.LENGTH_LONG).show();
		        		}
		        	}
		        });
				
			}
		});
        
        if(isNetworkAvailable(ProgramsAll.this))
		{
			th.start();
		}
		else
		{
			pd.dismiss();
			Toast.makeText(ProgramsAll.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
		}
	}
	
	private void parseProgramData() 
	{
		pd=new ProgressDialog(ProgramsAll.this);
        pd.setMessage("Loading...");
        pd.show();
        Thread th=new Thread(new Runnable() 
        {
			@Override
			public void run() 
			{
				http= new HttpConnection();
		        try
		        {
					http.getPrograms(new URL("http://upes.ac.in/service.asmx/data2"));
				} 
		        catch (MalformedURLException e)
		        {
					e.printStackTrace();
				}
		        catch (Exception e) 
		        {
					e.printStackTrace();
				}	
		        
		        data.deleteTable();
		        data.putCourseDetails();
		        data.putSemFee();
//		        getInfo();

		        runOnUiThread(new Runnable() {
		        	
		        	@Override
		        	public void run() {
		        		// TODO Auto-generated method stub
		        		pd.dismiss();
		        		if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
		        		{
		        			sort("cName", AppsConstant.vec_programInfo);
		        			listProgramAdapter=new ListProgramAdapter(AppsConstant.vec_programInfo);
		        			lvPrograms.setAdapter(listProgramAdapter);
		        			
		        		}
		        		else
		        		{
		        			Toast.makeText(ProgramsAll.this, "Please Check Your Network", Toast.LENGTH_LONG).show();
		        		}
		        	}
		        });
				
			}
		});
        
        if(isNetworkAvailable(ProgramsAll.this))
		{
			th.start();
		}
		else
		{
			pd.dismiss();
			Toast.makeText(ProgramsAll.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
		}
		
	}

	public void sort(final String field, Vector<ProgramInfo> item) 
	{
	    Collections.sort(item, new Comparator<ProgramInfo>() 
	   {
			@Override
			public int compare(ProgramInfo o2, ProgramInfo o1) 
			{
				   if(field.equals("cName")) 
				   {
		                return o2.cName.compareTo(o1.cName);
		           } 
				   else
				   {
					   return 0;
				   }
			}           
	    });
	}
	
	class ListProgramAdapter extends BaseAdapter
	{
		Vector<ProgramInfo> vec_prog_list;
		public ListProgramAdapter(Vector<ProgramInfo> vec) 
		{
			vec_prog_list = new Vector<ProgramInfo>();
			for(int i=0; i<vec.size(); i++)
				vec_prog_list.add(vec.get(i));
		}
		@Override
		public int getCount() 
		{
			return vec_prog_list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			convertView=(LinearLayout)(inflater).inflate(R.layout.list_programs, null);
			
			TextView tvProgName = (TextView)convertView.findViewById(R.id.tv_prog_name);
			
			tvProgName.setText(vec_prog_list.get(position).cName);
			convertView.setTag(position);
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					int pos = (Integer)v.getTag();
					AppsConstant.programDetails = vec_prog_list.get(pos);
					Intent intent = new Intent(ProgramsAll.this, ProgramsDetail.class);
					startActivity(intent);
					
				}
			});
			
			return convertView;
		}
		
	}
	
	
	
	
	
	public static boolean isNetworkAvailable(Context ctx)
	{
		ConnectivityManager cm =
		        (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

	    //NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		    NetworkInfo i = cm.getActiveNetworkInfo();
		    if (i == null)
		      return false;
		    if (!i.isConnected())
		      return false;
		    if (!i.isAvailable())
		      return false;
		    return true;

	}
	
	
	/*public int deleteTable()
    {
		int res = 0;
    	try 
    	{
    		res = _database.delete("course_details", null, null);
    	} 
    	catch (Exception e) 
    	{
    		res = 0;
    		Log.e("Error in while deleting", e.toString());
    	}
    	return res;
    }
	
	public long putCourseDetails()
    {
    	long res = 0;
    	for( int pos =0; pos<AppsConstant.vec_programInfo.size(); pos++)
    	{
    		ContentValues values = new ContentValues();
        	values.put("cName", AppsConstant.vec_programInfo.get(pos).cName);
        	values.put("sector", AppsConstant.vec_programInfo.get(pos).sector);
        	values.put("degree", AppsConstant.vec_programInfo.get(pos).degree);
        	values.put("pName", AppsConstant.vec_programInfo.get(pos).pName);
        	
        	try 
        	{
        		res = _database.insert("course_details", null, values);
        		Log.i("row inserted in Table ", "row id:" + res);
        	} 
        	catch (Exception e) 
        	{
        		res = 0;
        		Log.e("Error in while inserting", e.toString());
        	}
    	}
    	
    	return res;
    }*/
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		if(v == btnHeader)
		{
			Intent intent = new Intent(ProgramsAll.this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else if(v == imvPhone)
		{
			String number = "tel: +18001028737";
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
            startActivity(callIntent);
		}
		else if(v == btnAll)
		{
			btn = 1;
			AppsConstant.btnAll = true;
			btnAll.setBackgroundResource(R.drawable.btn1_press);
			btnSector.setBackgroundResource(R.drawable.btn2);
			btnDegree.setBackgroundResource(R.drawable.btn2);
			btnProg.setBackgroundResource(R.drawable.btn3);
			
			if(AppsConstant.vec_programInfo!=null && AppsConstant.vec_programInfo.size()>0)
    		{
				listProgramAdapter=new ListProgramAdapter(AppsConstant.vec_programInfo);
				lvPrograms.setAdapter(listProgramAdapter);
    		}
			else
			{
				Toast.makeText(ProgramsAll.this, "Please Check Your Network", Toast.LENGTH_LONG).show();
			}
			
		}
		else if(v == btnSector)
		{
			btn = 2;
			
			Intent intent = new Intent(ProgramsAll.this, ProgramList.class);
			intent.putExtra("prog_btn", btn);
			startActivity(intent);
			
		}
		else if(v == btnDegree)
		{
			btn = 3;
			
			Intent intent = new Intent(ProgramsAll.this, ProgramList.class);
			intent.putExtra("prog_btn", btn);
			startActivity(intent);
			
		}
		else if(v == btnProg)
		{
			btn = 4;
			
			Intent intent = new Intent(ProgramsAll.this, ProgramList.class);
			intent.putExtra("prog_btn", btn);
			startActivity(intent);
			
		}
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		AppsConstant.btnAll = true;
	}

}
