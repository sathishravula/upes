package com.upes;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Location extends BaseActivity implements OnClickListener{
	
	LayoutInflater inflater;
	LinearLayout llMainContent;	
	TextView tvTelCampus, tvTelOverseas, tvTelEnrollment, tvTelEnrollDelhi;
	TextView tvTelOfficeDelhi, tvTelOfficceMumbai, tvTelOfficceAhemadabad, tvTelOfficceKolkata, tvTelOfficceUsa ;
	TextView tvAuthChandigarh, tvAuthCoimbature, tvAuthKannur, tvAuthKochi, tvAuthVisak, tvAuthKottayam, tvAuthTrissur;
	TextView tvTelBookChandigarh, tvTelBookAllahabad, tvTelBookDelhi;
	
	TextView tvEmailOverseas, tvEmailEnrollment, tvEmailEnrollDelhi, tvEmailOfficeMumbai,tvEmailOfficeUSA;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.location, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Location");
		
		tvTelCampus = (TextView)llMainContent.findViewById(R.id.tv_tel_campus);
		tvTelOverseas = (TextView)llMainContent.findViewById(R.id.tv_tel_overseas);
		tvTelEnrollment = (TextView)llMainContent.findViewById(R.id.tv_tel_enrollment);
		tvTelEnrollDelhi = (TextView)llMainContent.findViewById(R.id.tv_tel_enrollment_delhi);
		
		tvTelOfficeDelhi = (TextView)llMainContent.findViewById(R.id.tv_tel_office_delhi);
		tvTelOfficceMumbai = (TextView)llMainContent.findViewById(R.id.tv_tel_office_mumbai);
		tvTelOfficceAhemadabad = (TextView)llMainContent.findViewById(R.id.tv_tel_office_ahemadabad);
		tvTelOfficceKolkata = (TextView)llMainContent.findViewById(R.id.tv_tel_office_kolkata);
		tvTelOfficceUsa = (TextView)llMainContent.findViewById(R.id.tv_tel_office_usa);
		
		tvAuthChandigarh = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_chandigarh);
		tvAuthCoimbature = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_coimbatore);
		tvAuthKannur = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_kannur);
		tvAuthKochi = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_kochi);
		tvAuthVisak = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_visak);
		tvAuthKottayam = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_kottayam);
		tvAuthTrissur = (TextView)llMainContent.findViewById(R.id.tv_tel_auth_thrissur);
		
		tvTelBookChandigarh = (TextView)llMainContent.findViewById(R.id.tv_tel_book_chandigarh);
		tvTelBookAllahabad = (TextView)llMainContent.findViewById(R.id.tv_tel_book_allahbad);
		tvTelBookDelhi = (TextView)llMainContent.findViewById(R.id.tv_tel_book_delhi);
		
		
		tvEmailOverseas = (TextView)llMainContent.findViewById(R.id.tv_email_overseas);
		tvEmailEnrollment = (TextView)llMainContent.findViewById(R.id.tv_email_enrollment);
		tvEmailEnrollDelhi = (TextView)llMainContent.findViewById(R.id.tv_email_enroll_delhi);
		tvEmailOfficeMumbai = (TextView)llMainContent.findViewById(R.id.tv_email_office_mumbai);
		tvEmailOfficeUSA = (TextView)llMainContent.findViewById(R.id.tv_email_office_usa);
		
		//tvEmailOverseas.setText("E-mail: "+Html.fromHtml("<font color= \"blue\">arungandhi48@yahoo.com</font>"));
		Spannable emailOverseas = new SpannableString(" E-mail: arungandhi48@yahoo.com");        
		emailOverseas.setSpan(new ForegroundColorSpan(Color.BLUE), 8, emailOverseas.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		Spannable enrollment = new SpannableString(" E-mail: enrollments@upes.ac.in");        
		enrollment.setSpan(new ForegroundColorSpan(Color.BLUE), 8, enrollment.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Spannable enrollDelhi = new SpannableString(" E-mail: enrollments@upes.ac.in");        
		enrollDelhi.setSpan(new ForegroundColorSpan(Color.BLUE), 8, enrollDelhi.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Spannable officeMumbai = new SpannableString(" E-mail: ArunJyoti@upes.ac.in");        
		officeMumbai.setSpan(new ForegroundColorSpan(Color.BLUE), 8, officeMumbai.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Spannable officeUSA = new SpannableString(" E-mail: arungandhi48@yahoo.com");        
		officeUSA.setSpan(new ForegroundColorSpan(Color.BLUE), 8, officeUSA.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		tvEmailOverseas.setText(emailOverseas);
		tvEmailEnrollment.setText(enrollment);
		tvEmailEnrollDelhi.setText(enrollDelhi);
		tvEmailOfficeMumbai.setText(officeMumbai);
		tvEmailOfficeUSA.setText(officeUSA);
		
		tvEmailOverseas.setOnClickListener(this);
		tvEmailEnrollDelhi.setOnClickListener(this);
		tvEmailEnrollment.setOnClickListener(this);
		tvEmailOfficeMumbai.setOnClickListener(this);
		tvEmailOfficeUSA.setOnClickListener(this);
		
		tvTelCampus .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelOverseas.setMovementMethod(LinkMovementMethod.getInstance());
		tvTelEnrollment.setMovementMethod(LinkMovementMethod.getInstance());
		tvTelEnrollDelhi.setMovementMethod(LinkMovementMethod.getInstance());
		
		tvTelOfficeDelhi .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelOfficceMumbai .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelOfficceAhemadabad .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelOfficceKolkata .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelOfficceUsa .setMovementMethod(LinkMovementMethod.getInstance());
		
		tvAuthChandigarh .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthCoimbature .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthKannur .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthKochi .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthVisak .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthKottayam .setMovementMethod(LinkMovementMethod.getInstance());
		tvAuthTrissur .setMovementMethod(LinkMovementMethod.getInstance());
		
		tvTelBookChandigarh .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelBookAllahabad .setMovementMethod(LinkMovementMethod.getInstance());
		tvTelBookDelhi .setMovementMethod(LinkMovementMethod.getInstance());
		
	}
	
	@Override
	public void onClick(View v) 
	{
		if(v == btnHeader)
		{
			Intent intent = new Intent(Location.this, HomeActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else if(v == imvPhone)
		{
			String number = "tel: +18001028737";
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
            startActivity(callIntent);
		}
		
		else if(v == tvEmailOverseas)
		{
			Intent i = new Intent(Intent.ACTION_SEND);
  			i.setType("plain/text");
  			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"arungandhi48@yahoo.com"});
  			try 
  			{
  				startActivity(Intent.createChooser(i, "Send mail..."));
  			} 
  			catch (android.content.ActivityNotFoundException ex) 
  			{
  			    Toast.makeText(Location.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
  			}
  			
		}
		
		else if(v == tvEmailEnrollment || v == tvEmailEnrollDelhi)
		{
			Intent i = new Intent(Intent.ACTION_SEND);
  			i.setType("plain/text");
  			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"enrollments@upes.ac.in"});
  			try 
  			{
  				startActivity(Intent.createChooser(i, "Send mail..."));
  			} 
  			catch (android.content.ActivityNotFoundException ex) 
  			{
  			    Toast.makeText(Location.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
  			}
  			
		}
		
		else if(v == tvEmailOfficeMumbai)
		{
			Intent i = new Intent(Intent.ACTION_SEND);
  			i.setType("plain/text");
  			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ArunJyoti@upes.ac.in"});
  			try 
  			{
  				startActivity(Intent.createChooser(i, "Send mail..."));
  			} 
  			catch (android.content.ActivityNotFoundException ex) 
  			{
  			    Toast.makeText(Location.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
  			}
  			
		}
		
		else if(v == tvEmailOfficeUSA)
		{
			Intent i = new Intent(Intent.ACTION_SEND);
  			i.setType("plain/text");
  			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"arungandhi48@yahoo.com"});
  			try 
  			{
  				startActivity(Intent.createChooser(i, "Send mail..."));
  			} 
  			catch (android.content.ActivityNotFoundException ex) 
  			{
  			    Toast.makeText(Location.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
  			}
  			
		}
		
		
	}

}
