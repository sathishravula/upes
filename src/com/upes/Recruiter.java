package com.upes;

import com.upes.object.RecruiterInfo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Recruiter extends BaseActivity{
	
	ListView lvRecruiter;
	ListRecruiterAdapter listRecruiterAdapter;
	LayoutInflater inflater;
	LinearLayout llMainContent;
	
	RecruiterInfo recruiterInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		recruiterInfo = new RecruiterInfo();
		setUpUI();
		
		// set up new UI
		
		listRecruiterAdapter=new ListRecruiterAdapter();
		lvRecruiter.setAdapter(listRecruiterAdapter);
		
		lvRecruiter.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				Intent intent = new Intent(Recruiter.this, RecruiterList.class);
				intent.putExtra("sector", arg2);
				startActivity(intent);
			}
		});
	}
	
	private void setUpUI() 
	{
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.recruiters, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Sectors");
        
        lvRecruiter = (ListView)llMainContent.findViewById(R.id.lv_recruiters);
		 
		
	}
	
	class ListRecruiterAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return recruiterInfo.sector.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			
			convertView=(LinearLayout)inflater.inflate(R.layout.list_items, null);
			
			TextView tvItems = (TextView)convertView.findViewById(R.id.tv_items);
			tvItems.setText(recruiterInfo.sector[position]);
			
			return convertView;
		}
		
	}

}
