package com.upes;

import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.connection.HttpConnection;
import com.upes.object.AppsConstant;

public class News extends BaseActivity {

  ListView lvNews;
  ListNewsAdapter listNewsAdapter;
  LayoutInflater inflater;
  RelativeLayout llMainContent;

  ProgressDialog pd;
  HttpConnection http;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);

    inflater = this.getLayoutInflater();
    llMainContent = new RelativeLayout(this);
    llMainContent = (RelativeLayout) inflater.inflate(R.layout.news, null);

    LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    llMain.addView(llMainContent, params);
    tvHeader.setText("Domain News");
    lvNews = (ListView) llMainContent.findViewById(R.id.lv_news);

    pd = new ProgressDialog(News.this);
    pd.setMessage("Loading...");
    pd.setCancelable(false);

    pd.show();
    Thread th = new Thread(new Runnable() {
      @Override
      public void run() {
        http = new HttpConnection();
        try {
          http.getNews(new URL("http://www.indiaprwire.com/syndication/oil-energy/"));
        } catch (MalformedURLException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }

        runOnUiThread(new Runnable() {

          @Override
          public void run() {
            // TODO Auto-generated method stub
            pd.dismiss();
            if (AppsConstant.vec_newsInfo != null && AppsConstant.vec_newsInfo.size() > 0) {
              listNewsAdapter = new ListNewsAdapter();
              lvNews.setAdapter(listNewsAdapter);
            }
          }
        });

      }
    });

    if (isNetworkAvailable(News.this)) {
      th.start();
    } else {
      pd.dismiss();
      Toast.makeText(News.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
    }

    lvNews.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                              long arg3) {
        // TODO Auto-generated method stub
        Intent intent = new Intent(News.this, NewsDescription.class);
        intent.putExtra("link", AppsConstant.vec_newsInfo.get(arg2).link);
        startActivity(intent);


//				String strURL = AppsConstant.vec_newsInfo.get(arg2).link;
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
//				startActivity(intent);


      }
    });

  }

  class ListNewsAdapter extends BaseAdapter {
    @Override
    public int getCount() {
      // TODO Auto-generated method stub
      return AppsConstant.vec_newsInfo.size();
    }

    @Override
    public Object getItem(int position) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public long getItemId(int position) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      convertView = inflater.inflate(R.layout.list_news, null);

      TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
      TextView tvNewsDesc = (TextView) convertView.findViewById(R.id.tv_news_desc);
      TextView tvPutDate = (TextView) convertView.findViewById(R.id.tv_put_date);

      tvTitle.setText(AppsConstant.vec_newsInfo.get(position).title);
      tvNewsDesc.setText(AppsConstant.vec_newsInfo.get(position).description);
      tvPutDate.setText(AppsConstant.vec_newsInfo.get(position).pubDate);

      return convertView;
    }

  }

}
