package com.upes;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.upes.object.RecruiterInfo;

public class RecruiterList extends BaseActivity implements OnClickListener{
	
	ListView lvRecruiter;
	ListRecruiterAdapter listRecruiterAdapter;
	LayoutInflater inflater;
	LinearLayout llMainContent;
	
	RecruiterInfo recruiterInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		
		inflater=this.getLayoutInflater();
		
		int pos = 0;
		pos = getIntent().getExtras().getInt("sector");
		
		recruiterInfo = new RecruiterInfo();
		
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.recruiters, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        
        tvHeader.setText("Recruiters");
        btnHeader.setVisibility(View.GONE);

		lvRecruiter = (ListView)llMainContent.findViewById(R.id.lv_recruiters);
		lvRecruiter.setSelector(android.R.color.transparent);
		
		switch (pos) {
		case 0:
			tvHeader.setText("Information System");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Information);
			break;
		case 1:
			tvHeader.setText("Infrastructure");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Infrastructure);
			
			break;
		case 2:
			tvHeader.setText("International Bussiness");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.International);
			
			break;
		case 3:
			tvHeader.setText("Logistics");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Logistics);
			break;
		case 4:
			tvHeader.setText("Oil & Gas");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Oil);
			break;
		case 5:
			tvHeader.setText("Power & Energy");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Power);
			break;
		case 6:
			tvHeader.setText("Transportation");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Transportation);
			break;

		default:
			tvHeader.setText("Information System");
			listRecruiterAdapter=new ListRecruiterAdapter(recruiterInfo.Information);
			break;
		}
		
		lvRecruiter.setAdapter(listRecruiterAdapter);
		
	}
	
	class ListRecruiterAdapter extends BaseAdapter
	{
		String[] listRecruiter;
		public ListRecruiterAdapter(String list[]) 
		{
			listRecruiter = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			Log.i("List recruiter", ""+listRecruiter.length);
			return listRecruiter.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			
			convertView=(LinearLayout)(inflater).inflate(R.layout.list_items, null);
			
			TextView tvItems = (TextView)convertView.findViewById(R.id.tv_items);
			tvItems.setText(listRecruiter[position]);
			
			return convertView;
		}
		
	}

}
