package com.upes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

import com.upes.object.AwardsInfo;

public class AwardsImage extends BaseActivity {
	
	LayoutInflater inflater;
	ScrollView slvMainContent;
		
	AwardsInfo awardsInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		slvMainContent = new ScrollView(this);
		slvMainContent = (ScrollView)inflater.inflate(R.layout.award_image, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(slvMainContent, params);
        
        btnHeader.setVisibility(View.GONE);
        
        ImageView imvAwardsPic = (ImageView)slvMainContent.findViewById(R.id.imv_award_pic); 
        TextView tvAwardDetails = (TextView)slvMainContent.findViewById(R.id.tv_award_details); 
        TextView tvAwardTitle = (TextView)slvMainContent.findViewById(R.id.tv_award_title); 
        
        awardsInfo = new AwardsInfo();
        
        int position = getIntent().getExtras().getInt("pos");
        
        tvHeader.setText(awardsInfo.tilte[position]);
        tvAwardTitle.setText(awardsInfo.tilte[position]);
        imvAwardsPic.setImageResource(awardsInfo.image_full[position]);
        tvAwardDetails.setText(awardsInfo.description[position]);
        
	}
	
}