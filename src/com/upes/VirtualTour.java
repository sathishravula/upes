package com.upes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

public class VirtualTour extends BaseActivity{
	
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	WebView web;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.virtual_tour, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Virtual Tour");
        
        
        web= (WebView)findViewById(R.id.web);
        
        WebSettings settings = web.getSettings();
		//settings.setBuiltInZoomControls(true);
		//settings.setUseWideViewPort(true);
		settings.setJavaScriptEnabled(true);
//		settings.setSupportMultipleWindows(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setLoadsImagesAutomatically(true);
		settings.setLightTouchEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setLoadWithOverviewMode(true);
        
	    settings.setPluginsEnabled(true);
	    settings.setAllowFileAccess(true);
		
//	    String html = "<object width=\"550\" height=\"400\"> <param name=\"movie\" value=\"http://upes.ac.in/cvt/tour.swf\"> <embed src=\"file:///sdcard/flash/2204355.swf\" width=\"550\" height=\"400\"> </embed> </object>";
//		String mimeType = "text/html";
//		String encoding = "utf-8";
//		
//	    web.loadDataWithBaseURL("null", html, mimeType, encoding, "");
	    
//	    if(Build.VERSION.SDK_INT >= 4.0)
//	    {
//	    	web.loadUrl("file:///android_asset/UPES_mobile_final_Android.html");
	        //this code will be executed o String html = "<object width=\"550\" height=\"400\"> <param name=\"movie\" value=\"http://upes.ac.in/cvt/tour.swf\"> <embed src=\"file:///sdcard/flash/2204355.swf\" width=\"550\" height=\"400\"> </embed> </object>";
//		String mimeType = "text/html";
//		String encoding = "utf-8";
//		
//	    web.ln devices running on DONUT (NOT ICS) or later
//	    }
//	    else
//	    {
	    	  web.loadUrl("file:///android_asset/tour.html");
	    	//web.loadUrl("http://upes.ac.in/cvt/tour.swf");
//	    }
	    	
	}
}
