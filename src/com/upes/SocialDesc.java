package com.upes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class SocialDesc extends BaseActivity implements OnClickListener{
	
	LayoutInflater inflater;
	RelativeLayout llMainContent;
	WebView web;
	ListView lvNews;
	ProgressDialog pd;
	String strURL = "";
	ProgressBar pg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();		
		llMainContent = new RelativeLayout(this);
		llMainContent = (RelativeLayout)inflater.inflate(R.layout.news, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Social");
        btnHeader.setVisibility(View.GONE);
        
        pg= (ProgressBar)llMainContent.findViewById(R.id.pgg);
        pg.setVisibility(View.VISIBLE);
        lvNews = (ListView)llMainContent.findViewById(R.id.lv_news);
		web= (WebView)llMainContent.findViewById(R.id.web_news);
		
		web.setVisibility(View.VISIBLE);
		lvNews.setVisibility(View.GONE);
		
		strURL = getIntent().getExtras().getString("link");
		web.getSettings().setJavaScriptEnabled(true);
		
		web.setWebViewClient(new WebViewClient()       
	    {
	         @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) 
	        {
	            return false;
	        }
	    });
		final Activity activity = this;
		web.setWebChromeClient(new WebChromeClient()
		{
		    public void onProgressChanged(WebView view, int progress) 
		    {
		        activity.setProgress(progress * 100);
		        if(progress==100)
		        	pg.setVisibility(View.GONE);
		    }
		    
		});
		
		web.getSettings().setDomStorageEnabled(true);
		web.loadUrl(strURL);
		
	}
		
			
}
