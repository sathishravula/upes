package com.upes;

import twitter4j.auth.AccessToken;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.codecube.facebook.FacebookLoginService;
import com.codecube.facebook.LoginButton;
import com.codecube.facebook.SessionStore;
import com.upes.object.AppsConstant;
import com.upes.object.ConnectionDetector;
import com.upes.twitter.TwitterApp;

public class ShareActivityDialog extends Activity implements OnClickListener {

  private Button btn_sms, btn_email, btn_facebook, btn_twitter;
  private String social_text = " Hello Upes testing";
  private FacebookLoginService facebookLoginService;
  private LoginButton mLoginButton;
  private ConnectionDetector connectionDetector;
  private ShareSocialText shareSocialText;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.dialog_share);
    connectionDetector = new ConnectionDetector(ShareActivityDialog.this);
    shareSocialText = new ShareSocialText(ShareActivityDialog.this);
    mLoginButton = new LoginButton(ShareActivityDialog.this);

    btn_sms = (Button) findViewById(R.id.button_sms);
    btn_sms.setOnClickListener(this);
    btn_email = (Button) findViewById(R.id.button_email);
    btn_email.setOnClickListener(this);

    btn_facebook = (Button) findViewById(R.id.button_facebook);
    btn_facebook.setOnClickListener(this);

    btn_twitter = (Button) findViewById(R.id.button_twitter);
    btn_twitter.setOnClickListener(this);


    /**
     * Handle OAuth Callback
     */
    Uri uri = getIntent().getData();
    if (uri != null && uri.toString().startsWith(AppsConstant.CALLBACK_URL)) {
      String verifier = uri.getQueryParameter(AppsConstant.IEXTRA_OAUTH_VERIFIER);
      try {
        AccessToken accessToken = TwitterApp.twitter.getOAuthAccessToken(TwitterApp.requestToken, verifier);
        Log.e("Accces Token Found", " " + accessToken.getToken());
        Log.e("Accces Token Found", " " + accessToken.getTokenSecret());
        TwitterApp.twitter.updateStatus(AppsConstant.Twitter_message);
      } catch (Exception e) {
        Log.e("Exception in call back", e.getMessage());
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
      }
      finish();
    }
  }


  @Override
  public void onClick(View v) {
    if (v.equals(btn_sms)) {
      shareViaSMS();
      ShareActivityDialog.this.finish();
    } else if (v.equals(btn_facebook)) {
      shareViaFacebook();
      ShareActivityDialog.this.finish();
    } else if (v.equals(btn_twitter)) {
      shareViaTwitter();
      ShareActivityDialog.this.finish();
    } else if (v.equals(btn_email)) {
      shareViaEmail();
      ShareActivityDialog.this.finish();
    }
  }


  /**
   * This Method is used to share via Email
   */
  private void shareViaEmail() {

    Intent sendIntent = new Intent(Intent.ACTION_SEND);
    sendIntent.setType("message/rfc822");

    sendIntent.setType("text/html");
    SpannableStringBuilder builder = new SpannableStringBuilder();
    builder.append("Try the UPES App!");

    sendIntent.putExtra(Intent.EXTRA_SUBJECT, " Try this UPES College Mobile App");
    sendIntent.putExtra(Intent.EXTRA_TEXT, builder);
    startActivity(Intent.createChooser(sendIntent, "Send mail..."));
  }


  /**
   * This Method is used to share via Email
   */
  private void shareViaSMS() {
    try {
      String smsBody = " Try the UPES App!";
      Intent sendsmsIntent = new Intent(Intent.ACTION_VIEW);
      SpannableStringBuilder builder1 = new SpannableStringBuilder();
      int start1 = builder1.length();
      builder1.append("");
      int end1 = builder1.length();
      sendsmsIntent.putExtra(Intent.EXTRA_TEXT, builder1);
      sendsmsIntent.putExtra("sms_body", smsBody);
      sendsmsIntent.setType("vnd.android-dir/mms-sms");
      startActivity(sendsmsIntent);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * This method invokes the facebook authentication
   */
  private boolean startFacebookAuthentication() {
    if (connectionDetector.isConnectingToInternet()) {
      SessionStore.clear(ShareActivityDialog.this);
      facebookLoginService = FacebookLoginService.getInstance(ShareActivityDialog.this);
      facebookLoginService.facebookLogin(ShareActivityDialog.this, mLoginButton);
      return true;
    } else {
      Toast.makeText(ShareActivityDialog.this, "Please check your internet conntection", Toast.LENGTH_SHORT).show();
      return false;
    }
  }


  /**
   * This Method is used to share via Email
   */
  private void shareViaFacebook() {
    startFacebookAuthentication();
  }


  /**
   * This Method is used to share via Email
   */
  private void shareViaTwitter() {
    Intent i = new Intent(ShareActivityDialog.this, com.upes.twitter.TwitterApp.class);
    startActivity(i);
  }
}
