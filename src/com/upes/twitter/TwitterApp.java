package com.upes.twitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.upes.R;
import com.upes.object.AppsConstant;
import com.upes.object.MyPreferences;

public class TwitterApp extends Activity {

	private static final String TAG = "Twitter Login example";

	public static Twitter twitter;
	public static RequestToken requestToken;
	private static SharedPreferences mSharedPreferences;
	private static TwitterStream twitterStream;
	private boolean running = false;
	private MyPreferences myPreferences;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.twitter_app_view);
		myPreferences = new MyPreferences(TwitterApp.this);
		askOAuth();
	}

	protected void onResume() {
		super.onResume();
		if (isConnected()) {
			try {
				TwitterApp.twitter.updateStatus("");
			} catch (TwitterException e) {
				Toast.makeText(TwitterApp.this, "Same post can not be posted again in same session. !! ", Toast.LENGTH_SHORT).show();
				Log.e("Twiiter Post Exception ae", " Twiter Exception : "+e);
			}catch (Exception ae){
				Log.e("Exception ae", " Exception : "+ae);
			}
		} 
	}



	/**
	 * check if the account is authorized
	 * @return
	 */
	private boolean isConnected() {
		String oauthAccessToken =  myPreferences.getTwitterToken(); 
		String oAuthAccessTokenSecret =  myPreferences.getTwitterSecretToken(); 

		if(oauthAccessToken!="" || oAuthAccessTokenSecret!=""){
			return true;
		}else{
			return false;
		}
	}


	private void askOAuth() {
		Log.e("inside Oath", "inside Oath");
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(AppsConstant.CONSUMER_KEY);
		configurationBuilder.setOAuthConsumerSecret(AppsConstant.CONSUMER_SECRET);
		Configuration configuration = configurationBuilder.build();
		twitter = new TwitterFactory(configuration).getInstance();
		try {
			requestToken = twitter.getOAuthRequestToken(AppsConstant.CALLBACK_URL);
			Toast.makeText(this, "Please authorize this app!", Toast.LENGTH_LONG).show();
			this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(requestToken.getAuthenticationURL())));
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
}
