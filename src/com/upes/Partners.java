package com.upes;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.upes.object.AppsConstant;

public class Partners extends BaseActivity implements OnClickListener{
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	ListView lvPartnerships;
	ListAdapter listAdapter;
	
	int country = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.about_us_detail2, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Partnerships");
        btnHeader.setVisibility(View.GONE);
        
        country = getIntent().getExtras().getInt("country_pos");
        
        lvPartnerships = (ListView)llMainContent.findViewById(R.id.lv_aboutus_detail);
        lvPartnerships.setSelector(android.R.color.transparent);
		
		if(AppsConstant.vec_partnershipInfo.get(country).vec_detail!=null && AppsConstant.vec_partnershipInfo.get(country).vec_detail.size()>0)
		{
			listAdapter=new ListAdapter();
			lvPartnerships.setAdapter(listAdapter);
			
		}
		
		
	}
	
	class ListAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AppsConstant.vec_partnershipInfo.get(country).vec_detail.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			convertView = inflater.inflate(R.layout.list_news, null);

			TextView tvUniversity = (TextView)convertView.findViewById(R.id.tv_title);
			TextView tvCity = (TextView)convertView.findViewById(R.id.tv_news_desc);
			TextView tvArea = (TextView)convertView.findViewById(R.id.tv_put_date);
			
			tvArea.setTextColor(Color.BLACK);
			tvUniversity.setText(AppsConstant.vec_partnershipInfo.get(country).vec_detail.get(position).university);
			tvCity.setText(AppsConstant.vec_partnershipInfo.get(country).vec_detail.get(position).city);
			tvArea.setText(AppsConstant.vec_partnershipInfo.get(country).vec_detail.get(position).area);
			
			return convertView;
		}
		
	}
	
}