package com.upes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.upes.connection.DatabaseHelper;

public class Splash extends Activity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.splash);
    doSplashWork();
  }


  public void doSplashWork() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
//						 public void createDataBase() 
//						    {

            DatabaseHelper dbHelper = new DatabaseHelper(Splash.this);
            try {
              dbHelper.createDataBase();
              DatabaseHelper.openDataBase();
            } catch (Exception e) {
              Log.e("Database Error MSG", e + "");
            }
//							}
            Intent intent = new Intent(Splash.this, HomeActivity.class);
            startActivity(intent);
            finish();
          }

        }, 2000);
      }
    });
  }

}
