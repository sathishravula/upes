package com.upes.object;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MyPreferences {

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor editor;


	// variables to store facebook token
	private String FACEBOOK_TOKEN = "facebook_token";
	private String TWITTER_TOKEN = "twitter_token";
	private String TWITTER_TOKEN_SECERET = "twitter_token_seceret";

	public MyPreferences(Context context) {
		mPreferences=PreferenceManager.getDefaultSharedPreferences(context);
		editor=mPreferences.edit();
	}

	public String getFacebookToken() {
		return mPreferences.getString(this.FACEBOOK_TOKEN, "");
	}


	public void setFacebookToken(String facebooktoken) {
		editor=mPreferences.edit();
		editor.putString(this.FACEBOOK_TOKEN, facebooktoken);
		editor.commit();
	}
	
	public String getTwitterToken() {
		return mPreferences.getString(this.TWITTER_TOKEN, "");
	}


	public void setTwitterToken(String Twittertoken) {
		editor=mPreferences.edit();
		editor.putString(this.TWITTER_TOKEN, Twittertoken);
		editor.commit();
	}
	
	
	public String getTwitterSecretToken() {
		return mPreferences.getString(this.TWITTER_TOKEN_SECERET, "");
	}


	public void setTwitterSecretToken(String TwitterSecrettoken) {
		editor=mPreferences.edit();
		editor.putString(this.TWITTER_TOKEN_SECERET, TwitterSecrettoken);
		editor.commit();
	}
}
