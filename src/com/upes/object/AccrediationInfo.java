package com.upes.object;

import com.upes.R;

public class AccrediationInfo {
	
	public String tilte[] = new String[]{"University Grants Commission" ,
											"National Assessment and Accreditation Council ",
											"Department of Scientific and Industrial Research", 
											"Distance Education Council, IGNOU",
											"Association of Indian Universities",
											"The Energy Institute, UK",
											"International Association of Universities",
											"ISO 9001:2008",
											"Bar Council of India (2007)" ,
											"Membership of Association of Commonwealth Universities(CUSAC)-2008"};
	
	public String description[] = 
				new String[]{"UPES is  recognized by University Grants Commission (UGC) under Section 2(f) of the UGC Act, 1956 and is the first university under Public-Private Partnership. \n\n UGC is a statutory body of the Government of India, established in 1956 through an Act of Parliament for coordination, determination and maintenance of standards of university education in India.",
							"UPES is accredited by National Assessment and Accreditation Council (NAAC) which is an autonomous body established by the University Grants Commission of India to access and accredit institutions of Higher Education in India ",
							"UPES is recognized as a \"Research Institution\" by the Department of Scientific and Industrial Research (DSIR), a part of the Ministry of Science and Technology, Government of India. This enables UPES to receive grants under the DSIR system.\n\nDSIR has a mandate to carry out the activities relating to indigenous technology promotion, development, utilization and transfer with the primary endeavour to promote R&D by the industries and various other institutions.",
							"UPES' Centre for Continuing Education (CCE) has obtained approval from the Distance Education Council for offering programmes through distance learning mode. \n\nThe Distance Education Council is a statutory body, part of the Indira Gandhi National Open University (IGNOU) for maintenance of standards in distance / open learning in India.",
							"UPES has been granted membership of the Association of Indian Universities (AIU), effective from November 2009. This entitles the university to various privileges like participation in inter-university sports, library and information services including subscription to University News, and assistance in recognition of degrees in addition to providing a platform for information exchange between the universities.",
							
							"UPES' oil and power sector courses are accredited by the Energy Institute, UK." +
							"\n\nThe Energy Institute, UK is the leading professional body for the energy industry, providing opportunities for career development, learning and networking. Representing almost 13500 individuals and 300 companies across 100 countries, the Institute provides a recognised community for those involved in all aspects of energy. This offers Indian Energy professionals access to global best practices, career development and networking opportunities.",
							
							"UPES is a member of the International Association of Universities (IAU). This enables the university to participate in IAU Annual Conferences, the General Conference, specialized workshops, seminars, experts meetings and various events. This also entitles UPES to receive regular reports on IAU activities and exchange information with the global higher education community.",
							
							"UPES is an ISO compliant institutions since 2005 and is presently certified as an ISO 9001:2008 institution. " +
							"\n\nInternational Organization for Standardization (ISO) is the world's largest developer and publisher of International Standards. ISO 9001:2008 specifies requirements for a quality management system where an organization needs to demonstrate its ability to : " +
							"\n\nconsistently provide a product that meets customer and applicable regulatory requirements," +
							"\n\nenhance customer satisfaction through the effective application of the system, including processes for continual improvement of the system, and" +
							"\n\nassure conformity to customer and applicable regulatory requirements.",
							
							"UPES Law Programs are accredited by the Bar Council of India." +
							"\n\nThe Bar Council of India is a statutory body that regulates and represents the Indian bar. It was created by Parliament under the Advocates Act, 1961. It prescribes standards of professional conduct and etiquette and exercises disciplinary jurisdiction. It sets standards for legal education and grants recognition to Universities whose degree serves as a qualification for students to enroll themselves as advocates upon graduation.",
							
							"UPES is also a member of the Commonwealth Universities Study Abroad Consortium (CUSAC), which is a group of universities formed around the Commonwealth who are committed to increasing internationalization at their institutions. " +
							"\n\nCUSAC functions as a network to facilitate the formation of partnerships to provide student exchange and study abroad opportunities, and also as a forum for members to share good practice and policy on all areas of internationalization. CUSAC was formed in 1993 with a small number of institutions. Membership expanded in 1999 and the scheme now has around 50 institutions, details of which can be found on this site. "};
	
	
	public int image[] = new int[]{R.drawable.blank,
							R.drawable.naac, 
							R.drawable.disr_logo,
							R.drawable.ignou,
							R.drawable.aiu_logo,
							R.drawable.energy_institute_india,
							R.drawable.international_association_universities_logo,
							R.drawable.dnv_logo,
							R.drawable.barcouncil,
							R.drawable.membership};

}
