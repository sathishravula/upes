package com.upes.object;

import com.upes.R;

public class AwardsInfo {
	
	
	public String tilte[] = new String[]{"GRIHA (National Green Building Rating system)",
											"Petrotech Lifetime Achievement Award 2012" ,
											"Outstanding Petroleum University",
											"Best Placement Brochure 2012", 
											"SAP ACE Award for Public Services 2012",
											"Industry Interface Award 2010",
											"Innovation Leadership Award 2010",
											"Academic Brand of the Year for Sectoral Specific Business School 2009",
											"Best B-School with Excellent Industry Interface 2009",
											"Best Professor Teaching Marketing Management 2009",
											"State Energy Conservation Award 2009",
											"Best Global Education / Training Provider 2008",
											"Recognizing Leadership & Excellence : Outstanding Achievement Institution 2008",
											"Outstanding Individual Achievement in the Energy Sector Award 2005"};
	
	public String description[] = 
				new String[]{"GRIHA (National Green Building Rating system) four star rating awarded to UPES Campus by Honorable President of India, Shri Pranab Mukherjee at a function in Vigyan Bhawan.",
							"Dr. S J Chopra, Chancellor, UPES is the recipient of the prestigious Petrotech Lifetime Achievement Award attributed for his contribution to the growth of Petroleum industry in India.",
							"Awarded at the 11th Annual World Oil Awards 2012 by World Oil Magazine and sponsored by Gulf Publishing Company at Houston, Texas USA ",
							"Awarded by INDY's Group of Stars of the Industry Group at the 6th Indy's Award Ceremony under the category of Indy's Best B-School Awards in Mumbai",
							"Special Recognition for Excellence in Higher Education & Research \n\nAwarded by SAP India to celebrate Customer Excellence and the power of SAP solutions to enable business competitiveness",
							"B-School with Excellent Industry Interface 2010 \n\nAwarded by B-School Affairs at Suntech, Singapore under the category of Asia's Best B-School Awards. ",
							"B-School with Innovation Leadership Award 2010 \n\nAwarded by B-School Affairs at Suntech, Singapore under the category of Asia's Best B-School Awards. ",
							"Awarded by Dewang Mehta Business School Awards on 5th November 2009 at Mumbai in recognition of UPES' assiduous effort in innovation, contemporary curriculum, industry interaction and placement efforts.\n\nDr. Parag Diwan, Vice Chancellor, UPES received the award from Mr. Ashish Chauhan, Deputy CEO, Bombay Stock Exchange.",
							"Awarded at the Dainik Bhaskar B School Leadership Awards in Mumbai.\n\nMr. Arun Jyoti, received the award on behalf of UPES from the Honourable Prime Minister of Bhutan, Jigme Yoser Thinley.",
							"Awarded to Dr. Suresh Malodia at the Dainik Bhaskar B School Leadership Awards in Mumbai. ",
							"2nd position for UPES (Office Building and Educational Institute category) \n1st prize for Dr. Kamal Bansal (BEE Certified Energy Auditor) for his work in conducting energy audits and training programs and for preparing technical literature on energy conservation awarded by the State Energy Conservation Award 2009 (Uttarakhand).\n\nThe awards were received by Prof. G. C. Tewari and Mr. Kamal Bansal, respectively, from the Honourable Principal Secretary Energy of Uttarakhand. ",
							"Awarded by: Getenergy, UK at London received by Dr. Parag Diwan, Vice Chancellor, UPES from Mr. Mike Jakins, Operations Training Coordinator, Chevron Australia ",
							"Awarded at Oceantex 2008 (15th February, 2008 at Mumbai)\n\nDr. Parag Diwan, Vice Chancellor, UPES, received the award from Mr. V. K. Sibal, Director General Hydrocarbons -OCEANTEX 2008. ",
							"Awarded to Mr. Sanjay Kaul, President, HERS (sponsoring body of UPES) by The Energy Institute, UK at London "};
	
	
	public int image[] = 
				 new int[]{	R.drawable.griha,
							R.drawable.petro, 
							R.drawable.woa,
							R.drawable.indy_award,
							R.drawable.sap_ace,
							R.drawable.best_b_school_awards,
							R.drawable.innovation,
							R.drawable.award_brand_leadership,
							R.drawable.excellent_industry_interface_award,
							R.drawable.teaching_marketing_management_award,
							R.drawable.state_energy_conservation_award,
							R.drawable.get_energy,
							R.drawable.parag_diwan,
							R.drawable.energy_sector_award};
	
	public int image_full[] = 
			 new int[]{	R.drawable.griha1,
						R.drawable.petro1, 
						R.drawable.woa1,
						R.drawable.indy_award1,
						R.drawable.sap_ace1,
						R.drawable.best_b_school_awards1,
						R.drawable.innovation1,
						R.drawable.award_brand_leadership1,
						R.drawable.excellent_industry_interface_award1,
						R.drawable.teaching_marketing_management_award1,
						R.drawable.state_energy_conservation_award1,
						R.drawable.get_energy1,
						R.drawable.parag_diwan1,
						R.drawable.energy_sector_award1};
	
}
