package com.upes.object;

import java.util.Vector;

public class PartnershipInfo
{
	public Vector<PartnershipInfo> vec_detail;
	
	public PartnershipInfo() 
	{
		vec_detail = new Vector<PartnershipInfo>();
	}
	public String country;
	public String university;
	public String city;
	public String area;

}
