package com.upes.object;

public class RecruiterInfo {
	
	public String [] sector = new String [] {"Information System", "Infrastructure","International Bussiness","Logistics","Oil & Gas","Power & Energy","Transportation" };
	
	public String [] Power
			= new String [] {"A2Z Group","Abhijeet Group","ABPS Infrastructure Advisory Pvt. Ltd.","Accenture",
				"Advance Metering Technology Ltd.","Alstom Power Projects", "APITCO Ltd. ","Atlanta Enterprises Pvt. Ltd.","BSES Rajdhani ","CESC Ltd. ",
				"Crompton Greaves","Dara Shaw ","Deloitte Touche Tohmatsu India Pvt. Ltd.","Delta Energy Systems","DMV Business & Market Research Pvt. Ltd.",
				"DRS Rooftech","Dun & Bradstreet'. ","E Gateways","E Value Serve ","Emerson Process Management ", "Enercon","EnKing  International",
				"Eon Electric Ltd", "Globaldata","GMR Group", "Green Mantra ","GreenTree Building Energy(P) Ltd. ", "Havells India Ltd. ","HCL Technologies Ltd.","Indian Energy Exchange","Infosys Tecnologies Ltd",
				"IT Power India Pvt. Limited","Jindal ITF","Kalpataru Power Transmission Limited","KSK Dibbin Hydro Power Pvt. Ltd.","L&TIT ","Lanco Infratech Limited","Larson & Toubro Limited","Macawber Beekay Pvt. Ltd.",
				"Mahindra Satyam ","MCX ","Noida Power Company Limited","North Delhi Power Limited","PricewaterhouseCoopers Pvt. Ltd.","PXIL","Raychem RPG Limited","RIL ADAG","Siemens","Thermax Ltd. ","Torrent Power Limited",
				"Turboatom-TPS Projects Ltd.","Videocon Industries ( Power Division) ","Vijai Electricals ","WAPCOS","Wartsila"};
	
	public String [] Oil 
			= new String [] {"Accenture","ACE Pipeline Construction Pvt.Ltd.",
				"Adani Ltd., Adani Gas Ltd, Adani Power","AFCONS Infrastructure",
				"Airliquide India Holdings Pvt ltd","ANEWA Engineering","Asian Oilfield services",
				"Baker Hughes","Beroe Inc","Bharat Oman Refineries Limited ","Bharat Petroleum Corporation Limited",
				"Bharat Shell","BHG Associates Pvt. Ltd.","BOTIL Oil Tools Pvt. Ltd.","British Gas ","Cairn Energy Ltd.",
				"Casewell Drilling","Castrol","CINDA Engineering & Construction Pvt. Ltd ","Corrtech Intl. ",
				"CRISIL","Deep Industries Ltd.","Deloitte Touche Tohmatsu India Pvt. Ltd.","Dolphin Offshore Enterprises (India) Ltd.",
				"DORF KETAL CHEMICALS (I) PVT. LTD. ","E Value Serve ","Emerson Process Management","Ernst & Young Pvt. Ltd.",
				"Essar Oil Limited","Feedback Ventures","Franks International Middle East (BVI) ltd","Fugro Survey (India) Pvt. Ltd.",
				"GAIL (India) Limited","Gammon India Ltd. ","GENPACT","Geo Enpro Petroleum Ltd.","Glencore India Pvt. Limited","GMR Group",
				"Gujarat Gas Company Limited","Gujarat State Petroleum Corporation","Gujarat State Petronet Ltd.","Gulf Oil Corporation Limited","Haldia Petrochemicals Limited",
				"Haldor Topsoe ","Hindustan Colas","Hindustan Oil Exploration Company Ltd.","Hindustan Petroleum Corporation Limited","Honeywell Automation India Limited",
				"HPCL Mittal ","Indian Oil Corporation Limited","Indian Oiltanking Limited","Indian Petro ","Indian School of Petroleum & Energy","Indorama","Infosys Technologies Ltd.",
				"Infotech Enterprises","Ingenero Technologies Pvt Ltd ","Ingenious Process Solutions Pvt Ltd","Inox India Ltd.","Interocean","Invensys Process Systems","Jindal Drilling & Industries Ltd.","Jindal Steel & Power Limited  ",
				"John Energy","Juno Bitumix ","kalpataru Pipeline division ","Kandla Energy and Chemical Ltd.","Kazstroy Service Infrastructure India Pvt. Ltd","KEC International Limited","Keppel FELS Offshore ","L&T ECC","L&T Infotech",
				"L&T Valdel Engineering Limited","Lanco Power Trading","Larson & Toubro Limited","Lummus Technology","Mahanagar Gas Limited","Maharashtra Natural Gas Limited","Mahindra Satyam","Mangalore Refinery & Petrochemicals Limited",
				"McKinsey Knowledge Centre","Mitchell Drilling International Pty Ltd.","Mittal Processors Pvt. Ltd","Motul Lubs","Nagarjuna Oil Corporation Limited","Newsco Asia ","Niko Resources Ltd.","O.W. Bunker and Trading (India) Pvt Ltd",
				"Oil Field Warehouse & Services","ONGC ","Parson Overseas ","Petronet LNG Limited","Phillips Carbon Black Limited","PricewaterhouseCoopers Pvt. Ltd.","PSL Ltd  ","Punj Lloyd Limited","Quanta Process Solutions Pvt Ltd","Raychem RPG Limited",
				"Relaince(ADAG)","Reliance Industries Limited ","Roxar- an Emerson group ","Sabarmati Gas Limited","Saipem Triune ","Schlumberger Asia Services Ltd.","Shell India Markets Pvt.Ltd","Shiv - Vani Oil & Gas Exploration Services Ltd. ",
				"SHV Energy Private Limited ","Siemens","Siti Energy Limited","Sterlite Technologies Ltd","TATA Consultancy Services ","Tata Projects Ltd.","Torrent Power Ltd.","Total Energy","TransGraph Consulting Private Limited","Transocean",
				"Tuff Drilling Private Limited","Uhde Engineering","Weatherford International Ltd. ","Wipro Technologies ","Zeppelin Systems India Pvt. Ltd."};
	
	
	public String [] Logistics
			= new String [] {"Abhijeet Group","Accenture","APL Logistics","BlueDart Express", "Celebi","CL Logistics ( NYK Line) ",
					"Columbia Asia India","Dastur Business & Technology Consulting.", "Drive India (DIESL)","DTDC","E Value Serve ", "Federal Mogul", "Future Supply Chain Solutions Limited",
					"GMR Group","Gujarat Heavy Chemicals Limited","Honda Motor India ","Honda Motorcycle and scooter India Ltd ","ITC Limited","J.M.Baxi ","JBM Ltd.","Jeena & Co ","Jindal Industries ",
					"Kuehne Nagel ","L&T","Mahindra & Mahindra ","Mahindra Logistics ","Mahindra Satyam","Oilfield Warehouse & Services Pvt. Ltd..","Sheryas Relay Systems Ltd","Torrent Pharma",
					"Toyota Kirloskar Motor","Trans Asian Shipping Services (P) Ltd.","Transport Corporation of India Ltd.","Transystem Logistics International ","Varuna Integrated Logistics"};
	
	public String [] Transportation
			= new String [] {"Celebi","Aeromatrix","Albatross CFS","APL & APL Logistics Ltd.","Asia Motor Works Ltd.","Bajaj Auto ","Caterpiller","Centre for Asia Pacific Aviation","Cummins","Deccan Charters",
				"Dhamra Port","Escort Ltd","FedEx India","GATI","GMR Group","Goodrich ","GVK Group","Halani Shipping Pvt.Ltd.","HBE Group  ","HCL Technologies","Hindustan Motors ","HITECHOS","HMSI","Honda motorcycle and scooters India Pvt ltd ",
				"Ignis Aerospace & Design","IndianOil Skytanking ltd","Indigo Airline","Infosys Technologies Limited","Infosys Technologies Limited","Infotech Enterprises ","Ingenious Process Solutions Ltd","Interocean","ITQ –InterGlobe  Technologies Quotient  ",
				"J.M. Baxi & Co.","Jai Bharat Maruti ","JBM Ltd.","Jet Airways","JK Tyres","Kale Consultants Limited","Kingfisher Airlines Limited","Larsen & Toubro","Maersk India Pvt. Ltd.","Mahindra & Mahindra Ltd., ","Mahindra 2 wheelers","Mahindra Satyam",
				"Mahindra Satyam","Maini Aerospace","Maruti Suzuki Ltd. ","Maruti Udyog Ltd.   ","Mindarika","Motherson  Sumi  Systems Ltd ","NATRIP – Icat ","NYK Line (India) Ltd.","Padmini  VNA Mechatronics Pvt Ltd ","Punjab Tractors Ltd","Sabre Holdings Pvt. Ltd",
				"Seatech Shipping & Projects India Pvt. Ltd., ","Sheryas Relay Systems Ltd. ","Shun Shing Group International Ltd","Spicejet","SPL Shipping Pvt. Ltd.","Suvidha Parklift","Swaraj Mazda ","TATA Consultancy Services ","TATA Consultancy Services ","Tata Motors",
				"TNT Cargo","Transasia Line ","Transport Corporation of India Ltd.","Transworld Group of Companies","TVS Motors","Tyco Electronics ","Volvo Eicher ","Wan Hai Lines Ltd."};
	
	public String [] International
			= new String [] {"Abhijeet Group","AIRA consulting","AMI INDIA LOGISTICS PVT LTD", "Columbia Asai India",
				"Drive India (DIESL)","E Value Serve ","Globaldata","Gupta Coal","HDFC Bank","Ignis Aerospace & Design",
				"India Bulls ","Oxylane Group","Sheryas Relay Systems Ltd","Shun Shing Group International Ltd",
				"Trans Asian Shipping Services (P) Ltd.","Transport Corporation of India Ltd."};
	
	public String [] Information
			= new String [] {"Accenture","Binary Semantics Ltd.","Compare Infotech Pvt. Ltd.","ETH", "L&T Infotech",
						"Mahindra Satyam","RAMTeCH Energy ", "Sabre Holdings","StraVis IT Solutions "};
	
	public String [] Infrastructure
			= new String [] {"Abhijeet Group","Assotech","Atlanta Enterprizes Pvt.Ltd.", "Brindavan Energy & Infra Pvt. Ltd.",
			"Construction Quality Rating Agency Pvt.Ltd(CQRA)", "E Value Serve ","Empire Industries Ltd.","Era Infra Engineering Ltd",
			"Everest Industries Ltd.","Excellence Engineering ","GVR Infra Services","Infotech Enterprises ","KEC International Ltd",
			"KMC Constructions Ltd", "L& T Ramboll Consulting Engineers Ltd.","OPG Power","Paharpur Cooling Towers ",
			"Sew Infrastructure Ltd."};
}
