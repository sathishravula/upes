package com.upes.object;

public class RssFeedInfo
{
	public String title;
	public String link;
	public String description;
	public String pubDate;
	public String source;

}
