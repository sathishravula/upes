package com.upes.object;

import java.util.Vector;

public class ProgramInfo {
	
	public Vector<ProgramInfo> vec_course_detail;
	
	public SemFeeInfo sem1;
	public SemFeeInfo sem2;
	public SemFeeInfo sem3;
	public SemFeeInfo sem4;
	public SemFeeInfo sem5;
	public SemFeeInfo sem6;
	public SemFeeInfo sem7;
	public SemFeeInfo sem8;
	public SemFeeInfo sem9;
	public SemFeeInfo sem10;
	public SemFeeInfo sem11;
	public SemFeeInfo sem12;
	
	public ProgramInfo() 
	{
		vec_course_detail = new Vector<ProgramInfo>();
		
	}
	
	public String pName;
	public String description;
	
	
	public String course;
	public String cName;
	public String sector;
	public String degree;
	public String college;
	public String overview;
	public String semesterWise;
	public String eligibility;
	public String admissionCriteria;
	public String duration;
	public String Fee;
	
	public int sem;
//	public String tutionfee;
//	public String servicefee;
//	public String total;
	
	public String apply;

}
