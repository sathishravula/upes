package com.upes;

import java.net.MalformedURLException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.upes.connection.HttpConnection;

public class Register extends BaseActivity {


  LayoutInflater inflater;
  LinearLayout llMainContent;
  //TextView tvMsg;
  EditText edtName, edtEmail, edtPhone, edtCity, edtQuery;
  Spinner spnProgram;
  String[] listProgram = new String[]{"Select Program", "MBA", "M.Tech", "BBA", "B.Tech", "LLB", "B.Tech+LLB(Hons)"};
  String name, email, phone, city, program, query;
  //boolean valid_no = false;
  Button btnRegister;

  ProgressDialog pd;
  HttpConnection http;
  int registerResult = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    inflater = this.getLayoutInflater();
    setUpUI();


    ArrayAdapter<String> programAdapter = new ArrayAdapter<String>(Register.this, android.R.layout.simple_spinner_item, listProgram);
    programAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spnProgram.setAdapter(programAdapter);

    spnProgram.setOnItemSelectedListener(new OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        if (arg2 > 0) {
          //tvMsg.setVisibility(View.GONE);
          program = listProgram[arg2];
        }
//				else
//				{
//					tvMsg.setVisibility(View.VISIBLE);
//	        		tvMsg.setText(" * Please select the program");
//				}
      }

      @Override
      public void onNothingSelected(AdapterView<?> arg0) {


      }
    });

/*		edtName.setOnFocusChangeListener(new OnFocusChangeListener() {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) 
	        {
	        	if (!hasFocus) 
	        	{
	        		name = edtName.getText().toString().trim();
	        		if(name.length()<=0)
		        	{
	        			showMessageAlert("Please enter the Name");
//		        		tvMsg.setVisibility(View.VISIBLE);
//		        		tvMsg.setText(" * Please enter the Name");
		        	}
//		        	else
//		        		tvMsg.setVisibility(View.GONE);
	        	}
	        	
	        }
	    });
		edtEmail.setOnFocusChangeListener(new OnFocusChangeListener() {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) 
	        {
	        	if (!hasFocus) 
	        	{
	        		email = edtEmail.getText().toString().trim();
	        		if(email.length()<=0)
		        	{
	        			showMessageAlert("Please enter the Email Id");
//		        		tvMsg.setVisibility(View.VISIBLE);
//		        		tvMsg.setText(" * Please enter the Email Id");
		        	}
		        	
	        		else if(!(email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") || email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+.[a-z]+")))
	        			showMessageAlert("Invalid Email Id");
		        		
		        		//tvMsg.setText(" * Invalid Email Id");
//		        	else
//		        		tvMsg.setVisibility(View.GONE);
	        		
	        	}
	        	
	        }
	    });
		
		edtPhone.setOnFocusChangeListener(new OnFocusChangeListener() {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) 
	        {
	        	if (!hasFocus) 
	        	{
	        		phone = edtPhone.getText().toString().trim();
	        		if(phone.length()<=0)
		        	{
	        			showMessageAlert("Please enter the Phone No.");
//		        		tvMsg.setVisibility(View.VISIBLE);
//		        		tvMsg.setText(" * Please enter the Phone No.");
		        	}
	        		else 
	        		{
	        			if(phone.startsWith("9", 0) || phone.startsWith("8", 0) || phone.startsWith("7", 0))
		        			valid_no = true;
		        		else
		        			valid_no = false;
	        			if(phone.length()<10 || !valid_no)
			        	{
		        			
		        			showMessageAlert("Invalid Phone No.");
			        	}
	        		}
	        	}
	        	
	        }
	    });
		
		edtCity.setOnFocusChangeListener(new OnFocusChangeListener() {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) 
	        {
	        	if (!hasFocus) 
	        	{
	        		city = edtCity.getText().toString().trim();
	        		if(city.length()<=0)
	        		{
	        			showMessageAlert("Please enter the city Name");
//	        			tvMsg.setVisibility(View.VISIBLE);
//	        			tvMsg.setText(" * Please enter the city Name");
	        		}
//	        		else
//	        			tvMsg.setVisibility(View.GONE);
	        	}
	        	
	        }
	    });
		
		edtQuery.setOnFocusChangeListener(new OnFocusChangeListener() {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) 
	        {
	        	if (!hasFocus) 
	        	{
	        		query = edtQuery.getText().toString().trim();
	        		if(query.length()<=0)
		        	{
	        			showMessageAlert("Please enter the query");
//		        		tvMsg.setVisibility(View.VISIBLE);
//		        		tvMsg.setText(" * Please enter the query");
		        	}
//		        	else
//		        		tvMsg.setVisibility(View.GONE);
	        	}
	        	
	        }
	    });*/

    btnRegister.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        checkField();
      }
    });

  }

  private void setUpUI() {
    llMainContent = new LinearLayout(this);
    llMainContent = (LinearLayout) inflater.inflate(R.layout.register, null);

    LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    llMain.addView(llMainContent, params);
    tvHeader.setText("Register");

    // tvMsg = (TextView)llMainContent.findViewById(R.id.tv_msg);
    edtName = (EditText) llMainContent.findViewById(R.id.edt_name);
    edtEmail = (EditText) llMainContent.findViewById(R.id.edt_email);
    edtPhone = (EditText) llMainContent.findViewById(R.id.edt_phone);
    edtCity = (EditText) llMainContent.findViewById(R.id.edt_city);
    spnProgram = (Spinner) llMainContent.findViewById(R.id.spn_program);
    edtQuery = (EditText) llMainContent.findViewById(R.id.edt_query);
    btnRegister = (Button) llMainContent.findViewById(R.id.btn_register);

    InputFilter filter = new InputFilter() {
      public CharSequence filter(CharSequence source, int start, int end,
                                 Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
          if (!Character.isLetter(source.charAt(i))) {
            return "";
          }
        }
        return null;
      }
    };

    edtName.setFilters(new InputFilter[]{filter});
    edtCity.setFilters(new InputFilter[]{filter});

  }

  protected void checkField() {
    name = edtName.getText().toString().trim();
    email = edtEmail.getText().toString().trim();
    phone = edtPhone.getText().toString().trim();
    city = edtCity.getText().toString().trim();
    query = edtQuery.getText().toString().trim();

    if (name.length() <= 0 || email.length() <= 0 || phone.length() <= 0 || city.length() <= 0 || query.length() <= 0) {
      showMessageAlert("All fields are mandatory");
    } else if (!(email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+") || email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+.[a-z]+"))) {
      showMessageAlert("Invalid Email Id.");
    } else if (phone.length() < 10) {
      showMessageAlert("Invalid Phone No.");
    } else if (spnProgram.getSelectedItemPosition() == 0) {
      showMessageAlert("Please Select Program");
    } else {
      register();
    }

  }

  protected void register() {
    pd = new ProgressDialog(Register.this);
    pd.setMessage("Please Wait...");
    pd.setCancelable(false);
    pd.show();
    Thread th = new Thread(new Runnable() {
      @Override
      public void run() {
        http = new HttpConnection();
        try {
          String params = "name=" + name + "&email=" + email + "&city=" + city + "&mobile=" + phone + "&comment=" + query + "&program=" + program;
          registerResult = http.getRegister("http://upes.ac.in/service.asmx/Stu_query?" + params);
        } catch (MalformedURLException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }

        runOnUiThread(new Runnable() {

          @Override
          public void run() {
            // TODO Auto-generated method stub
            pd.dismiss();
            if (registerResult == 1) {
              AlertDialog.Builder alrt = new AlertDialog.Builder(Register.this);
              alrt.setMessage("Successfully Submit.");
              alrt.setCancelable(false);

              alrt.setNeutralButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
                  finish();
                }
              });
              alrt.show();
            } else
              Toast.makeText(Register.this, "Error", Toast.LENGTH_LONG).show();

          }
        });

      }
    });

    if (isNetworkAvailable(Register.this)) {
      th.start();
    } else {
      pd.dismiss();
      Toast.makeText(Register.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();
    }
  }

}
