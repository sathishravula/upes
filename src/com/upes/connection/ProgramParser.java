package com.upes.connection;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.upes.object.AppsConstant;
import com.upes.object.ProgramInfo;
import com.upes.object.SemFeeInfo;


public class ProgramParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
	ProgramInfo ob;
	String progName, desc;
	int sem = 1;
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = "";
		currentElement = true;
		
		if (localName.equalsIgnoreCase("Table"))
		{			
			AppsConstant.vec_programInfo= new Vector<ProgramInfo>();
		}
     
		if (localName.equalsIgnoreCase("Course"))
		{
			ob= new ProgramInfo();
		}
		
		if (localName.equalsIgnoreCase("sem1"))
		{
			sem = 1;
			ob.sem1 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem2"))
		{
			sem = 2;
			ob.sem2 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem3"))
		{
			sem = 3;
			ob.sem3 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem4"))
		{
			sem = 4;
			ob.sem4 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem5"))
		{
			sem = 5;
			ob.sem5 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem6"))
		{
			sem = 6;
			ob.sem6 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem7"))
		{
			sem = 7;
			ob.sem7 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem8"))
		{
			sem = 8;
			ob.sem8 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem9"))
		{
			sem = 9;
			ob.sem9 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem10"))
		{
			sem = 10;
			ob.sem10 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem11"))
		{
			sem = 11;
			ob.sem11 = new SemFeeInfo();
		}
		if (localName.equalsIgnoreCase("sem12"))
		{
			sem = 12;
			ob.sem12 = new SemFeeInfo();
		}
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		currentElement = false;
		/** set value */ 
		if(localName.equalsIgnoreCase("PName"))
		{
			progName = currentValue;
		}
		
		else if(localName.equalsIgnoreCase("Description"))
		{
			desc = currentValue;
		}
				
		else if(localName.equalsIgnoreCase("CName"))
		{
			ob.cName=currentValue;
			ob.pName = progName;
			ob.description = desc;
		}
		
		else if(localName.equalsIgnoreCase("Sector"))
		{
			ob.sector=currentValue;
		}
		else if(localName.equalsIgnoreCase("Degree"))
		{
			ob.degree=currentValue;
		}
		else if(localName.equalsIgnoreCase("Overview"))
		{
			ob.overview=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("College"))
		{
			ob.college=currentValue;
		}
		else if(localName.equalsIgnoreCase("SemesterWise"))
		{
			ob.semesterWise = currentValue;
		}
		else if(localName.equalsIgnoreCase("Eligibility"))
		{
			ob.eligibility = currentValue;
		}
		else if(localName.equalsIgnoreCase("AdmissionCriteria"))
		{
			ob.admissionCriteria = currentValue;
		}
		else if(localName.equalsIgnoreCase("Duration"))
		{
			ob.duration = currentValue;
		}
		
		else if(localName.equalsIgnoreCase("sem1"))
		{
			ob.sem = 1;
			ob.sem1.sem = "Sem1";
		}
		else if(localName.equalsIgnoreCase("sem2"))
		{
			ob.sem = 2;
			ob.sem2.sem = "Sem2";
		}
		else if(localName.equalsIgnoreCase("sem3"))
		{
			ob.sem = 3;
			ob.sem3.sem = "Sem3";
		}
		else if(localName.equalsIgnoreCase("sem4"))
		{
			ob.sem = 4;
			ob.sem4.sem = "Sem4";
		}
		else if(localName.equalsIgnoreCase("sem5"))
		{
			ob.sem = 5;
			ob.sem5.sem = "Sem5";
		}
		else if(localName.equalsIgnoreCase("sem6"))
		{
			ob.sem = 6;
			ob.sem6.sem = "Sem6";
		}
		else if(localName.equalsIgnoreCase("sem7"))
		{
			ob.sem = 7;
			ob.sem7.sem = "Sem7";
		}
		else if(localName.equalsIgnoreCase("sem8"))
		{
			ob.sem = 8;
			ob.sem8.sem = "Sem8";
		}
		else if(localName.equalsIgnoreCase("sem9"))
		{
			ob.sem = 9;
			ob.sem9.sem = "Sem9";
		}
		else if(localName.equalsIgnoreCase("sem10"))
		{
			ob.sem = 10;
			ob.sem10.sem = "Sem10";
		}
		else if(localName.equalsIgnoreCase("sem11"))
		{
			ob.sem = 11;
			ob.sem11.sem = "Sem11";
		}
		else if(localName.equalsIgnoreCase("sem12"))
		{
			ob.sem = 12;
			ob.sem12.sem = "Sem12";
		}
		
		else if(localName.equalsIgnoreCase("tutionfee"))
		{
			if(sem == 1)
				ob.sem1.tutionfee = currentValue;
			else if(sem == 2)
				ob.sem2.tutionfee = currentValue;
			else if(sem == 3)
				ob.sem3.tutionfee = currentValue;
			else if(sem == 4)
				ob.sem4.tutionfee = currentValue;
			else if(sem == 5)
				ob.sem5.tutionfee = currentValue;
			else if(sem == 6)
				ob.sem6.tutionfee = currentValue;
			else if(sem == 7)
				ob.sem7.tutionfee = currentValue;
			else if(sem == 8)
				ob.sem8.tutionfee = currentValue;
			else if(sem == 9)
				ob.sem9.tutionfee = currentValue;
			else if(sem == 10)
				ob.sem10.tutionfee = currentValue;
			else if(sem == 11)
				ob.sem11.tutionfee = currentValue;
			else if(sem == 12)
				ob.sem12.tutionfee = currentValue;
			
		}
		else if(localName.equalsIgnoreCase("servicefee"))
		{
			if(sem == 1)
				ob.sem1.servicefee = currentValue;
			else if(sem == 2)
				ob.sem2.servicefee = currentValue;
			else if(sem == 3)
				ob.sem3.servicefee = currentValue;
			else if(sem == 4)
				ob.sem4.servicefee = currentValue;
			else if(sem == 5)
				ob.sem5.servicefee = currentValue;
			else if(sem == 6)
				ob.sem6.servicefee = currentValue;
			else if(sem == 7)
				ob.sem7.servicefee = currentValue;
			else if(sem == 8)
				ob.sem8.servicefee = currentValue;
			else if(sem == 9)
				ob.sem9.servicefee = currentValue;
			else if(sem == 10)
				ob.sem10.servicefee = currentValue;
			else if(sem == 11)
				ob.sem11.servicefee = currentValue;
			else if(sem == 12)
				ob.sem12.servicefee = currentValue;
		}
		else if(localName.equalsIgnoreCase("total"))
		{
			if(sem == 1)
				ob.sem1.total = currentValue;
			else if(sem == 2)
				ob.sem2.total = currentValue;
			else if(sem == 3)
				ob.sem3.total = currentValue;
			else if(sem == 4)
				ob.sem4.total = currentValue;
			else if(sem == 5)
				ob.sem5.total = currentValue;
			else if(sem == 6)
				ob.sem6.total = currentValue;
			else if(sem == 7)
				ob.sem7.total = currentValue;
			else if(sem == 8)
				ob.sem8.total = currentValue;
			else if(sem == 9)
				ob.sem9.total = currentValue;
			else if(sem == 10)
				ob.sem10.total = currentValue;
			else if(sem == 11)
				ob.sem11.total = currentValue;
			else if(sem == 12)
				ob.sem12.total = currentValue;
		}
		else if(localName.equalsIgnoreCase("Apply"))
		{
			ob.apply = currentValue;
		}
		
		else if(localName.equalsIgnoreCase("Course"))
		{
			AppsConstant.vec_programInfo.add(ob);
		}
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
			if (currentElement) 
			{
				currentValue += new String(ch, start, length);
			}
	} 

}
