package com.upes.connection;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.upes.object.AppsConstant;


public class TimeStampParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = "";
		currentElement = true;
		
		if (localName.equalsIgnoreCase("Table"))
		{		
			AppsConstant.parseDate = " ";
			AppsConstant.parseDates= " ";
		}
     
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		currentElement = false;
		/** set value */ 
		if(localName.equalsIgnoreCase("DATE"))
		{
			AppsConstant.parseDate = currentValue;
		}
		
		else if(localName.equalsIgnoreCase("DATES"))
		{
			AppsConstant.parseDates = currentValue;
		}
		
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
			if (currentElement) 
			{
				currentValue += new String(ch, start, length);
			}
	} 

}
