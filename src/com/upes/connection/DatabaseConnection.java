package com.upes.connection;

import java.util.Vector;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.upes.object.AppsConstant;
import com.upes.object.ProgramInfo;
import com.upes.object.SemFeeInfo;

public class DatabaseConnection 
{
	
	SQLiteDatabase _database = DatabaseHelper.openDataBase();
	
	public void getTimeStamp() 
	{
		Cursor c;
		String query = "Select * from Time_stamp"; 
				
		c = DatabaseHelper.get(query);
		if(c!=null)
		{
			if(c.moveToFirst())
		    {
				do {
					try 
					{
						AppsConstant.date = c.getString(0);
						AppsConstant.dates = c.getString(1);
					} 
					catch (Exception e)
					{
						Log.i("Error in getting data ", e.toString());
					}
						
				} while (c.moveToNext());
		    }
			c.close();
		}
	}
	
	/*public int updateTimeStamp()
    {
    	int res;
    	ContentValues values = new ContentValues();
    	values.put("date", AppsConstant.parseDate);
    	values.put("dates", AppsConstant.parseDates);
    	
    	try 
    	{
    		res = _database.update("Time_stamp", values, null, null );
    		Log.i("Number of rows updated : ", "" + res);
    	} 
    	catch (Exception e) 
    	{
    		res = 0;
//    		Log.e("Error in while updating", e.toString());
    	}
    	return res;
    }*/
	
	public int deleteTimeStamp()
    {
		int res = 0;
    	try 
    	{
    		res = _database.delete("Time_stamp", null, null);
    	} 
    	catch (Exception e) 
    	{
    		res = 0;
    		Log.e("Error in while deleting", e.toString());
    	}
    	return res;
    }
	
	public long putTimeStamp()
    {
    	long res;
    	ContentValues values = new ContentValues();
    	values.put("date", AppsConstant.parseDate);
    	values.put("dates", AppsConstant.parseDates);
    	
    	try 
    	{
    		res = _database.insert("Time_stamp", null, values);
    		Log.i("row inserted in time stamp",  res+"");
    	} 
    	catch (Exception e) 
    	{
    		res = 0;
    		Log.e("Error in while inserting", e.toString());
    	}
    	return res;
    }
	
	public void getCourseDetails() 
	{
		Cursor c;
		String query = "Select * from course_details A  join Fee_details B on A.c_id = B.c_id"; 
				
		c = DatabaseHelper.get(query);
		
		if (AppsConstant.vec_programInfo == null) 
			AppsConstant.vec_programInfo = new Vector<ProgramInfo>();
		else 
			AppsConstant.vec_programInfo.clear();
		
		if(c.moveToFirst())
	    {
			do {
				try 
				{
					ProgramInfo ob = new ProgramInfo();
					ob.cName = c.getString(0);
					ob.sector = c.getString(1);
					ob.degree = c.getString(2);
					ob.pName = c.getString(3);
					ob.description = c.getString(4);
					ob.college = c.getString(5);
					ob.overview = c.getString(6);
					ob.semesterWise = c.getString(7);
					ob.eligibility = c.getString(8);
					ob.admissionCriteria = c.getString(9);
					ob.duration = c.getString(10);
					ob.apply = c.getString(11);
					//c.getInt(12);
					if(c.getString(13)!=null && c.getString(13).length()>0 )
					{
						ob.sem1 = new SemFeeInfo();
						ob.sem1.tutionfee = c.getString(13);
						ob.sem1.servicefee = c.getString(25);
						ob.sem1.total = c.getString(37);
					}
					if(c.getString(14)!=null && c.getString(14).length()>0 )
					{
						ob.sem2 = new SemFeeInfo();
						ob.sem2.tutionfee = c.getString(14);
						ob.sem2.servicefee = c.getString(26);
						ob.sem2.total = c.getString(38);
					}
					if(c.getString(15)!=null && c.getString(15).length()>0 )
					{
						ob.sem3 = new SemFeeInfo();
						ob.sem3.tutionfee = c.getString(15);
						ob.sem3.servicefee = c.getString(27);
						ob.sem3.total = c.getString(39);
					}
					if(c.getString(16)!=null && c.getString(16).length()>0 )
					{
						ob.sem4 = new SemFeeInfo();
						ob.sem4.tutionfee = c.getString(16);
						ob.sem4.servicefee = c.getString(28);
						ob.sem4.total = c.getString(40);
					}
					if(c.getString(17)!=null && c.getString(17).length()>0 )
					{
						ob.sem5 = new SemFeeInfo();
						ob.sem5.tutionfee = c.getString(17);
						ob.sem5.servicefee = c.getString(29);
						ob.sem5.total = c.getString(41);
					}
					if(c.getString(18)!=null && c.getString(18).length()>0 )
					{
						ob.sem6 = new SemFeeInfo();
						ob.sem6.tutionfee = c.getString(18);
						ob.sem6.servicefee = c.getString(30);
						ob.sem6.total = c.getString(42);
					}
					if(c.getString(19)!=null && c.getString(19).length()>0 )
					{
						ob.sem7 = new SemFeeInfo();
						ob.sem7.tutionfee = c.getString(19);
						ob.sem7.servicefee = c.getString(31);
						ob.sem7.total = c.getString(43);
					}
					if(c.getString(20)!=null && c.getString(20).length()>0 )
					{
						ob.sem8 = new SemFeeInfo();
						ob.sem8.tutionfee = c.getString(20);
						ob.sem8.servicefee = c.getString(32);
						ob.sem8.total = c.getString(44);
					}
					
					if(c.getString(21)!=null && c.getString(21).length()>0 )
					{
						Log.i("sem9",c.getString(33));
						ob.sem9 = new SemFeeInfo();
						ob.sem9.tutionfee = c.getString(21);
						ob.sem9.servicefee = c.getString(33);
						ob.sem9.total = c.getString(45);
					}
					if(c.getString(22)!=null && c.getString(22).length()>0 )
					{
						Log.i("sem10",c.getString(34));
						ob.sem10 = new SemFeeInfo();
						ob.sem10.tutionfee = c.getString(22);
						ob.sem10.servicefee = c.getString(34);
						ob.sem10.total = c.getString(46);
					}
					if(c.getString(23)!=null && c.getString(23).length()>0 )
					{
						ob.sem11 = new SemFeeInfo();
						ob.sem11.tutionfee = c.getString(23);
						ob.sem11.servicefee = c.getString(35);
						ob.sem11.total = c.getString(47);
					}
					if(c.getString(24)!=null && c.getString(24).length()>0 )
					{
						ob.sem12 = new SemFeeInfo();
						ob.sem12.tutionfee = c.getString(24);
						ob.sem12.servicefee = c.getString(36);
						ob.sem12.total = c.getString(48);
					}
					
					AppsConstant.vec_programInfo .add(ob);
				} 
				catch (Exception e)
				{
					Log.i("Error in getting data ", e.toString());
				}
					
			} while (c.moveToNext());
	    }
		c.close();

	}
	
	public int deleteTable()
    {
		int res = 0;
    	try 
    	{
    		res = _database.delete("course_details", null, null);
    	} 
    	catch (Exception e) 
    	{
    		res = 0;
    		Log.e("Error in while deleting", e.toString());
    	}
    	return res;
    }
	
	
	public long putCourseDetails()
    {
    	long res = 0;
    	for( int pos =0; pos<AppsConstant.vec_programInfo.size(); pos++)
    	{
    		ContentValues values = new ContentValues();
        	values.put("cName", AppsConstant.vec_programInfo.get(pos).cName);
        	values.put("sector", AppsConstant.vec_programInfo.get(pos).sector);
        	values.put("degree", AppsConstant.vec_programInfo.get(pos).degree);
        	values.put("pName", AppsConstant.vec_programInfo.get(pos).pName);
        	
        	values.put("Description", AppsConstant.vec_programInfo.get(pos).description);
        	values.put("College", AppsConstant.vec_programInfo.get(pos).college);
        	values.put("Overview", AppsConstant.vec_programInfo.get(pos).overview);
        	values.put("SemesterWise", AppsConstant.vec_programInfo.get(pos).semesterWise);
        	values.put("Eligibility", AppsConstant.vec_programInfo.get(pos).eligibility);
        	values.put("AdmissionCriteria", AppsConstant.vec_programInfo.get(pos).admissionCriteria);
        	values.put("Duration", AppsConstant.vec_programInfo.get(pos).duration);
        	values.put("Apply", AppsConstant.vec_programInfo.get(pos).apply);
        	values.put("c_id", pos);
        	
        	try 
        	{
        		res = _database.insert("course_details", null, values);
        		//Log.i("row inserted in Table course ", "row id:" + res);
        	} 
        	catch (Exception e) 
        	{
        		res = 0;
        		Log.e("Error in while inserting", e.toString());
        	}
    	}
    	
    	return res;
    }
	
	public long putSemFee()
    {
    	long res = 0;
    	for( int pos =0; pos<AppsConstant.vec_programInfo.size(); pos++)
    	{
    		ContentValues values = new ContentValues();
    		
    		values.put("c_id", pos);
    		if(AppsConstant.vec_programInfo.get(pos).sem1!=null)
    		{
    			values.put("tutionfee1", AppsConstant.vec_programInfo.get(pos).sem1.tutionfee);
	        	values.put("servicefee1", AppsConstant.vec_programInfo.get(pos).sem1.servicefee);
	        	values.put("total1", AppsConstant.vec_programInfo.get(pos).sem1.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem2!=null)
    		{
    			values.put("tutionfee2", AppsConstant.vec_programInfo.get(pos).sem2.tutionfee);
	        	values.put("servicefee2", AppsConstant.vec_programInfo.get(pos).sem2.servicefee);
	        	values.put("total2", AppsConstant.vec_programInfo.get(pos).sem2.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem3!=null)
    		{
    			values.put("tutionfee3", AppsConstant.vec_programInfo.get(pos).sem3.tutionfee);
	        	values.put("servicefee3", AppsConstant.vec_programInfo.get(pos).sem3.servicefee);
	        	values.put("total3", AppsConstant.vec_programInfo.get(pos).sem3.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem4!=null)
    		{
    			values.put("tutionfee4", AppsConstant.vec_programInfo.get(pos).sem4.tutionfee);
	        	values.put("servicefee4", AppsConstant.vec_programInfo.get(pos).sem4.servicefee);
	        	values.put("total4", AppsConstant.vec_programInfo.get(pos).sem4.total);
    		}
    		
    		if(AppsConstant.vec_programInfo.get(pos).sem5!=null)
    		{
    			values.put("tutionfee5", AppsConstant.vec_programInfo.get(pos).sem5.tutionfee);
	        	values.put("servicefee5", AppsConstant.vec_programInfo.get(pos).sem5.servicefee);
	        	values.put("total5", AppsConstant.vec_programInfo.get(pos).sem5.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem6!=null)
    		{
    			values.put("tutionfee6", AppsConstant.vec_programInfo.get(pos).sem6.tutionfee);
	        	values.put("servicefee6", AppsConstant.vec_programInfo.get(pos).sem6.servicefee);
	        	values.put("total6", AppsConstant.vec_programInfo.get(pos).sem6.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem7!=null)
    		{
    			values.put("tutionfee7", AppsConstant.vec_programInfo.get(pos).sem7.tutionfee);
	        	values.put("servicefee7", AppsConstant.vec_programInfo.get(pos).sem7.servicefee);
	        	values.put("total7", AppsConstant.vec_programInfo.get(pos).sem7.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem8!=null)
    		{
    			values.put("tutionfee8", AppsConstant.vec_programInfo.get(pos).sem8.tutionfee);
	        	values.put("servicefee8", AppsConstant.vec_programInfo.get(pos).sem8.servicefee);
	        	values.put("total8", AppsConstant.vec_programInfo.get(pos).sem8.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem9!=null)
    		{
//    			Log.i(AppsConstant.vec_programInfo.get(pos).cName+"tution fee", AppsConstant.vec_programInfo.get(pos).sem9.tutionfee);
//    			Log.i("put service fee", AppsConstant.vec_programInfo.get(pos).sem1.servicefee);
//    			
    			values.put("tutionfee9", AppsConstant.vec_programInfo.get(pos).sem9.tutionfee);
	        	values.put("servicefee9", AppsConstant.vec_programInfo.get(pos).sem9.servicefee);
	        	values.put("total9", AppsConstant.vec_programInfo.get(pos).sem9.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem10!=null)
    		{
    			values.put("tutionfee10", AppsConstant.vec_programInfo.get(pos).sem10.tutionfee);
	        	values.put("servicefee10", AppsConstant.vec_programInfo.get(pos).sem10.servicefee);
	        	values.put("total10", AppsConstant.vec_programInfo.get(pos).sem10.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem11!=null)
    		{
    			values.put("tutionfee11", AppsConstant.vec_programInfo.get(pos).sem11.tutionfee);
	        	values.put("servicefee11", AppsConstant.vec_programInfo.get(pos).sem11.servicefee);
	        	values.put("total11", AppsConstant.vec_programInfo.get(pos).sem11.total);
    		}
    		if(AppsConstant.vec_programInfo.get(pos).sem12!=null)
    		{
    			values.put("tutionfee12", AppsConstant.vec_programInfo.get(pos).sem12.tutionfee);
	        	values.put("servicefee12", AppsConstant.vec_programInfo.get(pos).sem12.servicefee);
	        	values.put("total12", AppsConstant.vec_programInfo.get(pos).sem12.total);
    		}
	        	
        	try 
        	{
        		res = _database.insert("Fee_details", null, values);
        		//Log.i("row inserted in Fee Table ", "row id:" + res);
        	} 
        	catch (Exception e) 
        	{
        		res = 0;
        		Log.e("Error in while inserting fee details", e.toString());
        	}
    		
    	}
    	
    	return res;
    }

}
