package com.upes.connection;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.upes.object.AppsConstant;
import com.upes.object.PartnershipInfo;


public class PartnershipParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
	PartnershipInfo ob, partnerDetail;
	
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = "";
		
		if (localName.equalsIgnoreCase("Partnerships"))
		{			
			AppsConstant.vec_partnershipInfo= new Vector<PartnershipInfo>();
		}
     
		if (localName.equalsIgnoreCase("name"))
		{
			currentElement = true;
			ob= new PartnershipInfo();
		}
		
		if (localName.equalsIgnoreCase("detail"))
		{
			partnerDetail = new PartnershipInfo();
		}
		
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		
		/** set value */ 
		if(localName.equalsIgnoreCase("country"))
		{
			Log.i("Country", currentValue);
			ob.country=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("university"))
		{
			partnerDetail.university=currentValue;
		}
				
		else if(localName.equalsIgnoreCase("city"))
		{
			partnerDetail.city=currentValue;
		}
		else if(localName.equalsIgnoreCase("area"))
		{
			partnerDetail.area=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("detail"))
		{
			ob.vec_detail.add(partnerDetail);
		}
		else if(localName.equalsIgnoreCase("name"))
		{
			currentElement = false;
			AppsConstant.vec_partnershipInfo.add(ob);
		}
		
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		if (currentElement) 
		{
			currentValue += new String(ch, start, length);
		}
	} 

}
