package com.upes.connection;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.upes.object.AppsConstant;
import com.upes.object.RssFeedInfo;


public class NewsParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
	RssFeedInfo ob;
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = new String();
		currentElement = true;
		
		if (localName.equalsIgnoreCase("channel"))
		{			
			AppsConstant.vec_newsInfo= new Vector<RssFeedInfo>();
			ob= new RssFeedInfo();
			
		}
     
		if (localName.equalsIgnoreCase("item"))
		{
			ob= new RssFeedInfo();
		}
		
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		currentElement = false;
		
		/** set value */ 
		if(localName.equalsIgnoreCase("title"))
		{
			ob.title=currentValue;
			Log.i("News title ", ""+currentValue);
		}
		
		else if(localName.equalsIgnoreCase("link"))
		{
			ob.link=currentValue;
			Log.i("News link ", ""+currentValue);
		}
				
		else if(localName.equalsIgnoreCase("description"))
		{
			ob.description=currentValue;
			Log.i("News description ", ""+currentValue);
		}
		else if(localName.equalsIgnoreCase("pubDate"))
		{
			ob.pubDate=currentValue;
			Log.i("News date ", ""+currentValue);
		}
		else if(localName.equalsIgnoreCase("source"))
		{
			ob.source=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("item"))
		{
//			currentElement = false;
			AppsConstant.vec_newsInfo.add(ob);
			Log.i("News Vector size", ""+AppsConstant.vec_newsInfo.size());
		}
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		if (currentElement) 
		{
			currentValue += new String(ch, start, length);
			
		}
	} 

}
