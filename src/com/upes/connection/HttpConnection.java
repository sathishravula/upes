package com.upes.connection;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.util.Log;

public class HttpConnection {

  private HttpURLConnection connection;
  private static final int TIMEOUT_CONNECT_MILLIS = 6 * 60000;
  private static final int TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;
  int result;
  Context _mContext;
  private InputStream inStream;

  //Send coupon code to server

  public void sendCouponCode(String url, String code) throws Exception {
    try {
      HttpClient httpclient = new DefaultHttpClient();
      HttpPost httppost = new HttpPost(url);
      //httppost.setHeader("Content-Type","x-www-form-urlencoded");
      ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
      parameters.add(new BasicNameValuePair("coupon", code));
      httppost.setEntity(new UrlEncodedFormEntity(parameters));
      HttpResponse response = httpclient.execute(httppost);
      HttpEntity entity = response.getEntity();
      InputStream is = entity.getContent();
      InputStreamReader reader = new InputStreamReader(is);
      StringBuilder buf = new StringBuilder();
      char[] cbuf = new char[2048];
      int num;
      while (-1 != (num = reader.read(cbuf))) {
        buf.append(cbuf, 0, num);
      }
      String result = buf.toString();
      Log.v("Coupon response", result);
    } catch (MalformedURLException e) {
      Log.i("exception in sms", "" + e.toString());
      throw e;
    } catch (Exception e) {
      Log.i("exception in sms", "" + e.toString());
      throw e;
    }

  }

  // parse weather Info

  public void getweatherDetails(String strUrl) throws Exception {
    try {
      HttpClient httpclient = new DefaultHttpClient();
      HttpGet httppost = new HttpGet(strUrl);

      HttpResponse response = httpclient.execute(httppost);
      HttpEntity entity = response.getEntity();
      InputStream is = entity.getContent();

      SAXParserFactory spf = SAXParserFactory.newInstance();
      SAXParser sp = spf.newSAXParser();
      XMLReader xr = sp.getXMLReader();

      WeatherParser2 myXMLHandler = new WeatherParser2();
      xr.setContentHandler(myXMLHandler);
      xr.parse(new InputSource(is));

    } catch (MalformedURLException e) {
      Log.i("exception in sms", "" + e.toString());
      throw e;
    } catch (Exception e) {
      Log.i("exception in sms", "" + e.toString());
      throw e;
    }

  }

  // parse the Time Stamp
  public void getTimeStamp(URL url) throws Exception {
    try {
      connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.setDoOutput(true);
      connection.setUseCaches(false);

      connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
      connection.setReadTimeout(TIMEOUT_READ_MILLIS);
      connection.setRequestMethod("GET");

      inStream = (InputStream) connection.getInputStream();

      SAXParserFactory spf = SAXParserFactory.newInstance();
      SAXParser sp = spf.newSAXParser();
      XMLReader xr = sp.getXMLReader();

      TimeStampParser myXMLHandler = new TimeStampParser();
      xr.setContentHandler(myXMLHandler);
      xr.parse(new InputSource(inStream));

    } catch (MalformedURLException e) {

      Log.i("exception in sms", "" + e.toString());
      throw e;
    } catch (Exception e) {
      inStream = (InputStream) connection.getErrorStream();
      Log.i("exception in sms", "" + e.toString());
      throw e;
    }

  }

  // parse the program Details
  public void getPrograms(URL url) throws Exception {
    try {
      connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.setDoOutput(true);
      connection.setUseCaches(false);

      connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
      connection.setReadTimeout(TIMEOUT_READ_MILLIS);
      connection.setRequestMethod("GET");

      inStream = (InputStream) connection.getInputStream();

      SAXParserFactory spf = SAXParserFactory.newInstance();
      SAXParser sp = spf.newSAXParser();
      XMLReader xr = sp.getXMLReader();

      ProgramParser myXMLHandler = new ProgramParser();
      xr.setContentHandler(myXMLHandler);
      xr.parse(new InputSource(inStream));

    } catch (MalformedURLException e) {

      Log.i("exception in sms", "" + e.toString());
      throw e;
    } catch (Exception e) {
      inStream = (InputStream) connection.getErrorStream();
      Log.i("exception in sms", "" + e.toString());
      throw e;
    }

  }

  // parse the News
  public void getNews(URL url) throws Exception {
    try {
      connection = (HttpURLConnection) url.openConnection();
      connection.setDoInput(true);
      connection.setDoOutput(true);
      connection.setUseCaches(false);

      connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
      connection.setReadTimeout(TIMEOUT_READ_MILLIS);
      connection.setRequestMethod("GET");

      inStream = (InputStream) connection.getInputStream();

      SAXParserFactory spf = SAXParserFactory.newInstance();
      SAXParser sp = spf.newSAXParser();
      XMLReader xr = sp.getXMLReader();

      NewsParser myXMLHandler = new NewsParser();
      xr.setContentHandler(myXMLHandler);
      xr.parse(new InputSource(inStream));

    } catch (MalformedURLException e) {

      Log.i("exception in sms", "" + e.toString());
      throw e;
    } catch (Exception e) {

      Log.i("exception in sms", "" + e.toString());
      throw e;
    }

  }

  // parse the Social Page Details
  public void getSocial(String strUrl) throws Exception {

    try {
      HttpClient httpclient = new DefaultHttpClient();
      HttpGet httppost = new HttpGet(strUrl);

      HttpResponse response = httpclient.execute(httppost);
      HttpEntity entity = response.getEntity();
      InputStream is = entity.getContent();

      SAXParserFactory spf = SAXParserFactory.newInstance();
      SAXParser sp = spf.newSAXParser();
      XMLReader xr = sp.getXMLReader();

      SocialParser myXMLHandler = new SocialParser();
      xr.setContentHandler(myXMLHandler);
      xr.parse(new InputSource(is));


    } catch (UnsupportedEncodingException e) {
      Log.w(" error ", e.toString());
    } catch (Exception e) {
      Log.i("Exception in Image uploading", e.toString());

    }


  }

  // parse the Registration Info
  public int getRegister(String strUrl) throws Exception {


    try {
      HttpClient httpclient = new DefaultHttpClient();
      HttpGet httppost = new HttpGet(strUrl);

      HttpResponse response = httpclient.execute(httppost);
      HttpEntity entity = response.getEntity();

      InputStream is = entity.getContent();

      InputStreamReader reader = new InputStreamReader(is);
      StringBuilder buf = new StringBuilder();
      char[] cbuf = new char[2048];
      int num;
      while (-1 != (num = reader.read(cbuf))) {
        buf.append(cbuf, 0, num);
      }
      String result = buf.toString();
      System.err.println("Response sme sent = " + result);
      Log.i("Registerion", "" + result);
      reader.close();

      if (result.contains("1"))
        return 1;
      else
        return 0;


    } catch (UnsupportedEncodingException e) {
      Log.w(" error ", e.toString());
      return 0;
    } catch (Exception e) {
      Log.i("Exception in Image uploading", e.toString());
      return 0;

    }


  }

}
