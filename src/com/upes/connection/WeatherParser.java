package com.upes.connection;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.upes.object.AppsConstant;
import com.upes.object.WeatherDetail;


public class WeatherParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = "";
		currentElement = true;
		
		if (localName.equalsIgnoreCase("data"))
		{		
			AppsConstant.weatherDetail = new WeatherDetail();
		}
		
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		currentElement = false;
		if(localName.equalsIgnoreCase("temp_C"))
		{
			AppsConstant.weatherDetail.temp_C=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("date"))
		{
			AppsConstant.weatherDetail.date=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("tempMaxC"))
		{
			AppsConstant.weatherDetail.tempMaxC=currentValue;
		}
		else if(localName.equalsIgnoreCase("tempMinC"))
		{
			AppsConstant.weatherDetail.tempMinC=currentValue;
		}
		else if(localName.equalsIgnoreCase("weatherDesc"))
		{
			AppsConstant.weatherDetail.weatherDesc=currentValue;
		}
				
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		if (currentElement) 
		{
			currentValue += new String(ch, start, length);
		}
	} 

}
