package com.upes.connection;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.upes.object.AppsConstant;
import com.upes.object.RssFeedInfo;


public class SocialParser extends DefaultHandler{
	
	String currentValue;
	boolean currentElement;
	RssFeedInfo ob;
		
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException 
	{
		currentValue = "";
		currentElement = true;
		
		if (localName.equalsIgnoreCase("channel"))
		{			
			AppsConstant.vec_socialInfo= new Vector<RssFeedInfo>();
			ob= new RssFeedInfo();
		}
     
		if (localName.equalsIgnoreCase("item"))
		{
			ob= new RssFeedInfo();
		}
		
	}
	@Override
	public void endElement(String uri, String localName, String qName)throws SAXException 
	{
		currentElement = false;
		
		/** set value */ 
		if(localName.equalsIgnoreCase("title"))
		{
			ob.title=currentValue;
		}
		
		else if(localName.equalsIgnoreCase("link"))
		{
			ob.link=currentValue;
		}
				
		else if(localName.equalsIgnoreCase("description"))
		{
			ob.description=currentValue;
		}
		else if(localName.equalsIgnoreCase("pubDate"))
		{
			ob.pubDate=currentValue;
		}
				
		else if(localName.equalsIgnoreCase("item"))
		{
			AppsConstant.vec_socialInfo.add(ob);
		}
	}
	@Override
	public void characters(char[] ch, int start, int length)throws SAXException 
	{
		if (currentElement) 
		{
			currentValue = new String(ch, start, length);
			currentElement = false;
		}
	} 

}
