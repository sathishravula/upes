package com.upes.connection;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.upes.object.AppsConstant;
import com.upes.object.WeatherDetail;


public class WeatherParser2 extends DefaultHandler {

  String currentValue;
  boolean currentElement;
  boolean isForecast = true;

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    currentValue = "";
    currentElement = true;

    if (localName.equalsIgnoreCase("item")) {
      AppsConstant.weatherDetail = new WeatherDetail();
    }

    if (localName.equalsIgnoreCase("condition")) {
      String strTemp = attributes.getValue("temp");
      String strDesc = attributes.getValue("text");

      double t = Double.parseDouble(strTemp);
      t = (0.555555556) * (t - 32);
      strTemp = String.valueOf((int) t);

      AppsConstant.weatherDetail.temp_C = strTemp;
      AppsConstant.weatherDetail.weatherDesc = strDesc;
    }

    if (localName.equalsIgnoreCase("forecast")) {
      String strHigh = attributes.getValue("high");
      String strLow = attributes.getValue("low");

      if (isForecast) {
        double high = Double.parseDouble(strHigh);
        double low = Double.parseDouble(strLow);
        high = (0.555555556) * (high - 32);
        low = (0.555555556) * (low - 32);

        strHigh = String.valueOf((int) high);
        strLow = String.valueOf((int) low);

        AppsConstant.weatherDetail.tempMaxC = strHigh;
        AppsConstant.weatherDetail.tempMinC = strLow;
        isForecast = false;
      }


    }

  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    currentElement = false;

    if (localName.equalsIgnoreCase("pubDate")) {
      AppsConstant.weatherDetail.date = currentValue;
    }

//		if(localName.equalsIgnoreCase("temp_C"))
//		{
//			AppsConstant.weatherDetail.temp_C=currentValue;
//		}

//		else if(localName.equalsIgnoreCase("date"))
//		{
//			AppsConstant.weatherDetail.date=currentValue;
//		}

//		else if(localName.equalsIgnoreCase("tempMaxC"))
//		{
//			AppsConstant.weatherDetail.tempMaxC=currentValue;
//		}
//		else if(localName.equalsIgnoreCase("tempMinC"))
//		{
//			AppsConstant.weatherDetail.tempMinC=currentValue;
//		}
//		else if(localName.equalsIgnoreCase("weatherDesc"))
//		{
//			AppsConstant.weatherDetail.weatherDesc=currentValue;
//		}

  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement) {
      currentValue += new String(ch, start, length);
    }
  }

}
