package com.upes;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AboutUs extends BaseActivity {

  LayoutInflater inflater;
  LinearLayout llMainContent;
  TextView tvWho, tvVision, tvAccrediation, tvPartner, tvAwards, tvChancellor, tvVC;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    inflater = this.getLayoutInflater();
    setUpUI();

  }

  private void setUpUI() {
    llMainContent = new LinearLayout(this);
    llMainContent = (LinearLayout) inflater.inflate(R.layout.about_us, null);

    LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    llMain.addView(llMainContent, params);
    tvHeader.setText("About Us");

    tvWho = (TextView) llMainContent.findViewById(R.id.tv_who);
    tvVision = (TextView) llMainContent.findViewById(R.id.tv_vision);
    tvAccrediation = (TextView) llMainContent.findViewById(R.id.tv_accrediation);
    tvPartner = (TextView) llMainContent.findViewById(R.id.tv_partner);
    tvAwards = (TextView) llMainContent.findViewById(R.id.tv_award);
    tvChancellor = (TextView) llMainContent.findViewById(R.id.tv_chancellor);
    tvVC = (TextView) llMainContent.findViewById(R.id.tv_vc);

    tvWho.setOnClickListener(this);
    tvVision.setOnClickListener(this);
    tvAccrediation.setOnClickListener(this);
    tvPartner.setOnClickListener(this);
    tvAwards.setOnClickListener(this);
    tvChancellor.setOnClickListener(this);
    tvVC.setOnClickListener(this);

  }

  @Override
  public void onClick(View v) {
    if (v == btnHeader) {
      Intent intent = new Intent(AboutUs.this, HomeActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
    } else if (v == imvPhone) {
      String number = "tel: +18001028737";
      Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
      startActivity(callIntent);
    } else if (v == tvWho) {
      Intent intent = new Intent(AboutUs.this, AboutWho.class);
      startActivity(intent);
    } else if (v == tvVision) {
      Intent intent = new Intent(AboutUs.this, AboutVision.class);
      startActivity(intent);
    } else if (v == tvAccrediation) {
      Intent intent = new Intent(AboutUs.this, Accrediation.class);
      startActivity(intent);
    } else if (v == tvPartner) {
      Intent intent = new Intent(AboutUs.this, Partnership.class);
      startActivity(intent);
    } else if (v == tvAwards) {
      Intent intent = new Intent(AboutUs.this, Awards.class);
      startActivity(intent);
    } else if (v == tvChancellor) {
      Intent intent = new Intent(AboutUs.this, AboutUsDetail.class);
      intent.putExtra("about", 1);
      startActivity(intent);
    } else if (v == tvVC) {
      Intent intent = new Intent(AboutUs.this, AboutUsDetail.class);
      intent.putExtra("about", 2);
      startActivity(intent);
    }
  }
}
