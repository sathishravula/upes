package com.upes;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.object.AppsConstant;

public class ProgramsDetail extends BaseActivity {
	
	LayoutInflater inflater;
	ScrollView slvMainContent;
	
	TextView tvProgram, tvOverview, tvDuration, tvEligibility;
	WebView webSem, webFee;
	TableLayout table;
	int sem, position;
	int screenWidth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		setUpUI();
		
		Display display = getWindowManager().getDefaultDisplay(); 
        screenWidth = display.getWidth()-30;  // deprecated
        //screenHeigth = display.getHeight();
        
		if(AppsConstant.programDetails !=null )
		{
			String overview=" ",duration=" ", eligibility=" ", semesterwise= " ";
			
			if(AppsConstant.programDetails.overview !=null && AppsConstant.programDetails.overview.length()>0 )
			{
				overview = AppsConstant.programDetails.overview.replaceAll("\\<.*?>","");
				overview = overview.replaceAll("&nbsp;","");
				overview = overview.replaceAll("&amp;","&");
			}
			if(AppsConstant.programDetails.duration !=null && AppsConstant.programDetails.duration.length()>0 )
			{
				duration = AppsConstant.programDetails.duration.replaceAll("\\<.*?>","");
				duration = duration.replace("&nbsp;","");
				duration = duration.replace("&amp;","&");
			}
			if(AppsConstant.programDetails.eligibility !=null && AppsConstant.programDetails.eligibility.length()>0 )
			{
				eligibility = AppsConstant.programDetails.eligibility.replaceAll("\\<.*?>","");
				eligibility = eligibility.replace("&nbsp;","");
				eligibility = eligibility.replace("&amp;","&");
			}
			
			tvProgram.setText(AppsConstant.programDetails.cName);
			tvOverview.setText(overview.trim());
			tvDuration.setText(duration.trim());
			tvEligibility.setText(eligibility.trim());
			
			if(AppsConstant.programDetails.semesterWise !=null && AppsConstant.programDetails.semesterWise.length()>0 )
				semesterwise = AppsConstant.programDetails.semesterWise;
			
			String fee = feeDetails();
//			webSem.loadData(new String(semesterwise), "text/html", "UTF-8");
			
			try 
			{
				webSem.loadData(URLEncoder.encode(semesterwise,"utf-8").replaceAll("\\+"," "), "text/html", "utf-8");
			} 
			catch (UnsupportedEncodingException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			webFee.loadData(new String(fee), "text/html", "UTF-8");
		}
		else
		{
			Toast.makeText(ProgramsDetail.this, "Please Check Network Connection & Try Again.", Toast.LENGTH_LONG).show();
		}
		
	}
	
	private String feeDetails() 
	{
		String strFee="<Html><style>td {padding:4px;}</style><table border = 1 width ="+screenWidth+">" +
						"<tr>" +
						"<td><b>Semester</b></td>" +
						"<td><b>Tution Fee</b></td>" +
						"<td><b>Service</b></td>" +
						"<td><b>Total Fee</b></td>" +
						"</tr>";
		if(AppsConstant.programDetails.sem1!=null)
		{
			String sem1 = "<tr>" +
					"<td><b>Semester&nbsp;1</b></td>" +
					"<td>"+AppsConstant.programDetails.sem1.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem1.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem1.total +"</td>" +
					"</tr>";
			strFee = strFee+sem1;
		}
		if(AppsConstant.programDetails.sem2!=null)
		{
			String sem2 = "<tr>" +
					"<td><b>Semester&nbsp;2</b></td>" +
					"<td>"+AppsConstant.programDetails.sem2.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem2.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem2.total +"</td>" +
					"</tr>";
			strFee = strFee+sem2;
			
		}
		if(AppsConstant.programDetails.sem3!=null)
		{
			String sem3 = "<tr>" +
					"<td><b>Semester&nbsp;3</b></td>" +
					"<td>"+AppsConstant.programDetails.sem3.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem3.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem3.total +"</td>" +
					"</tr>";
			strFee = strFee+sem3;
		}
		if(AppsConstant.programDetails.sem4!=null)
		{
			String sem4 = "<tr>" +
					"<td><b>Semester&nbsp;4</b></td>" +
					"<td>"+AppsConstant.programDetails.sem4.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem4.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem4.total +"</td>" +
					"</tr>";
			strFee = strFee+sem4;
		}
		if(AppsConstant.programDetails.sem5!=null)
		{
			String sem5 = "<tr>" +
					"<td><b>Semester&nbsp;5</b></td>" +
					"<td>"+AppsConstant.programDetails.sem5.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem5.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem5.total +"</td>" +
					"</tr>";
			strFee = strFee+sem5;
		}
		if(AppsConstant.programDetails.sem6!=null)
		{
			String sem6 = "<tr>" +
					"<td><b>Semester&nbsp;6</b></td>" +
					"<td>"+AppsConstant.programDetails.sem6.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem6.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem6.total +"</td>" +
					"</tr>";
			strFee = strFee+sem6;
		}
		if(AppsConstant.programDetails.sem7!=null)
		{
			String sem7 = "<tr>" +
					"<td><b>Semester&nbsp;7</b></td>" +
					"<td>"+AppsConstant.programDetails.sem7.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem7.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem7.total +"</td>" +
					"</tr>";
			strFee = strFee+sem7;
		}
		if(AppsConstant.programDetails.sem8!=null)
		{
			String sem8 = "<tr>" +
					"<td><b>Semester&nbsp;8</b></td>" +
					"<td>"+AppsConstant.programDetails.sem8.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem8.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem8.total +"</td>" +
					"</tr>";
			strFee = strFee+sem8;
		}
		
		if(AppsConstant.programDetails.sem9!=null)
		{
			String sem9 = "<tr>" +
					"<td><b>Semester&nbsp;9</b></td>" +
					"<td>"+AppsConstant.programDetails.sem9.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem9.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem9.total +"</td>" +
					"</tr>";
			strFee = strFee+sem9;
		}
		if(AppsConstant.programDetails.sem10!=null)
		{
			String sem10 = "<tr>" +
					"<td><b>Semester&nbsp;10</b></td>" +
					"<td>"+AppsConstant.programDetails.sem10.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem10.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem10.total +"</td>" +
					"</tr>";
			strFee = strFee+sem10;
		}
		if(AppsConstant.programDetails.sem11!=null)
		{
			String sem11 = "<tr>" +
					"<td><b>Semester&nbsp;11</b></td>" +
					"<td>"+AppsConstant.programDetails.sem11.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem11.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem11.total +"</td>" +
					"</tr>";
			strFee = strFee+sem11;
		}
		if(AppsConstant.programDetails.sem12!=null)
		{
			String sem12 = "<tr>" +
					"<td><b>Semester&nbsp;12</b></td>" +
					"<td>"+AppsConstant.programDetails.sem12.tutionfee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem12.servicefee +"</td>" +
					"<td>"+AppsConstant.programDetails.sem12.total +"</td>" +
					"</tr>";
			strFee = strFee+sem12;
		}
		
		String last = "</table> </Html>";
		strFee = strFee+last;
		
		return strFee;
	}

	private void setUpUI() 
	{
		slvMainContent = new ScrollView(this);
		slvMainContent = (ScrollView)inflater.inflate(R.layout.programs_detail, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(slvMainContent, params);
        tvHeader.setText("Course Details");
        
        btnHeader.setVisibility(View.GONE);
        
		tvProgram = (TextView)slvMainContent.findViewById(R.id.tv_title_program);
		tvOverview = (TextView)slvMainContent.findViewById(R.id.tv_overview);
		tvDuration = (TextView)slvMainContent.findViewById(R.id.tv_duration);
		tvEligibility = (TextView)slvMainContent.findViewById(R.id.tv_eligibility);
		webSem = (WebView)slvMainContent.findViewById(R.id.web_semesterwise);
		webFee = (WebView)slvMainContent.findViewById(R.id.web_fee);
		
		
	}

}