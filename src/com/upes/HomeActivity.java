package com.upes;

import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.upes.connection.HttpConnection;
import com.upes.object.AppsConstant;

public class HomeActivity extends Activity implements OnClickListener {

  HttpConnection http;
  ImageView imvPhone, imvPrograms, imvMaps, imvRegister, imvRecruiter, imvVirtualTour, imvNews, imvLocation, imvSocial, imvAboutUs;
  TextView tvDehradoon, tvDay, tvLowValue, tvHighValue, tvTemp, tvTempDesc, tvCouponCode;
  private Button share_button;
  long couponLong;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.home);

    setUpUI();
    TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//		String imei = tm.getDeviceId();
//		couponLong = Long.valueOf(imei);
//		Log.v("Coupon","The coupon is "+Long.toString(couponLong, 36));
//		String couponString = "To gain from this number <b>"+Long.toString(couponLong, 36)+"</b><br/>visit www.upes.ac.in/mobileapp.htm";
//		tvCouponCode.setText(Html.fromHtml(couponString));

    imvPhone.setOnClickListener(this);
    imvPrograms.setOnClickListener(this);
    imvMaps.setOnClickListener(this);
    imvRegister.setOnClickListener(this);
    imvRecruiter.setOnClickListener(this);
    imvVirtualTour.setOnClickListener(this);
    imvNews.setOnClickListener(this);
    imvLocation.setOnClickListener(this);
    imvSocial.setOnClickListener(this);
    imvAboutUs.setOnClickListener(this);
  }

  private void setUpUI() {
    share_button = (Button) findViewById(R.id.button_home_share);
    share_button.setOnClickListener(this);

    tvDehradoon = (TextView) findViewById(R.id.tv_dehradoon);
    tvDay = (TextView) findViewById(R.id.tv_day);
    tvLowValue = (TextView) findViewById(R.id.tv_low_value);
    tvHighValue = (TextView) findViewById(R.id.tv_high_value);
    tvTemp = (TextView) findViewById(R.id.tv_temp);
    tvTempDesc = (TextView) findViewById(R.id.tv_temp_desc);
    tvCouponCode = (TextView) findViewById(R.id.couponTextView);

    imvPhone = (ImageView) findViewById(R.id.imv_phone1);
    imvPrograms = (ImageView) findViewById(R.id.imv_programs);
    imvMaps = (ImageView) findViewById(R.id.imv_maps);
    imvRegister = (ImageView) findViewById(R.id.imv_register);
    imvRecruiter = (ImageView) findViewById(R.id.imv_recruiter);
    imvVirtualTour = (ImageView) findViewById(R.id.imv_virtual_tour);
    imvNews = (ImageView) findViewById(R.id.imv_news);
    imvLocation = (ImageView) findViewById(R.id.imv_location);
    imvSocial = (ImageView) findViewById(R.id.imv_social);
    imvAboutUs = (ImageView) findViewById(R.id.imv_about_us);


    Thread th = new Thread(new Runnable() {
      @Override
      public void run() {
        http = new HttpConnection();
        String strUrl = "http://query.yahooapis.com/v1/public/yql?q=select%20item%20from%20weather.forecast%20where%20location%3D%22INXX0140%22&format=xml";
        //"http://free.worldweatheronline.com/feed/weather.ashx?q=Dehradun,India&format=xml&num_of_days=1&key=13f15be11b065942130901";
        try {
          http.getweatherDetails(strUrl);
          //http.sendCouponCode("http://192.168.2.70:8888/rest/coupon", Long.toString(couponLong, 36));
//					http.sendCouponCode("http://cce.upes.ac.in/rest/coupon", Long.toString(couponLong, 36));
        } catch (MalformedURLException e) {
          e.printStackTrace();
        } catch (Exception e) {
          e.printStackTrace();
        }

        runOnUiThread(new Runnable() {

          @Override
          public void run() {
            // TODO Auto-generated method stub
            if (AppsConstant.weatherDetail != null) {
//		        			SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy");
//		    				Date date = new Date();
//		    				String strDate = dateFormat.format(date);
//		        			
//		        			tvDay.setText(strDate);
              if (AppsConstant.weatherDetail.date != null && AppsConstant.weatherDetail.date.length() > 0)
                tvDay.setText(AppsConstant.weatherDetail.date.substring(0, 16));

              if (AppsConstant.weatherDetail.tempMinC != null && AppsConstant.weatherDetail.tempMinC.length() > 0)
                tvLowValue.setText(AppsConstant.weatherDetail.tempMinC + (char) 0x00B0 + "C");
              if (AppsConstant.weatherDetail.tempMaxC != null && AppsConstant.weatherDetail.tempMaxC.length() > 0)
                tvHighValue.setText(AppsConstant.weatherDetail.tempMaxC + (char) 0x00B0 + "C");

              if (AppsConstant.weatherDetail.temp_C != null && AppsConstant.weatherDetail.temp_C.length() > 0)
                tvTemp.setText(AppsConstant.weatherDetail.temp_C + (char) 0x00B0 + "C");
              if (AppsConstant.weatherDetail.weatherDesc != null && AppsConstant.weatherDetail.weatherDesc.length() > 0)
                tvTempDesc.setText(AppsConstant.weatherDetail.weatherDesc);
            }
          }
        });

      }
    });

    if (isNetworkAvailable(HomeActivity.this)) {
      th.start();
    } else {
      Toast.makeText(HomeActivity.this, "Internet connection unavailable, Please try again later", Toast.LENGTH_LONG).show();

    }

  }

  @Override
  public void onClick(View v) {
    if (v == imvPhone) {
      String number = "tel: +18001028737";
      Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
      startActivity(callIntent);
    } else if (v == imvPrograms) {
      Intent intent = new Intent(HomeActivity.this, ProgramsAll.class);
      startActivity(intent);
    } else if (v == imvMaps) {
      if (isNetworkAvailable(HomeActivity.this)) {
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/"));
        Intent intent = new Intent(HomeActivity.this, GoogleMapActivity.class);
        startActivity(intent);
      } else
        Toast.makeText(HomeActivity.this, "Please Connect to Internet", Toast.LENGTH_LONG).show();

    } else if (v == imvRegister) {
      Intent intent = new Intent(HomeActivity.this, Register.class);
      startActivity(intent);
    } else if (v == imvRecruiter) {
      Intent intent = new Intent(HomeActivity.this, Recruiter.class);
      startActivity(intent);
    } else if (v == imvVirtualTour) {
      Intent intent = new Intent(HomeActivity.this, VirtualTour.class);
      startActivity(intent);
    } else if (v == imvNews) {
      Intent intent = new Intent(HomeActivity.this, News.class);
      startActivity(intent);
    } else if (v == imvLocation) {
      Intent intent = new Intent(HomeActivity.this, Location.class);
      startActivity(intent);
    } else if (v == imvSocial) {
      Intent intent = new Intent(HomeActivity.this, Social.class);
      startActivity(intent);
    } else if (v == imvAboutUs) {
      Intent intent = new Intent(HomeActivity.this, AboutUs.class);
      startActivity(intent);

    } else if (v.equals(share_button)) {
      Intent i = new Intent(HomeActivity.this, ShareActivityDialog.class);
      startActivity(i);
    }

  }

  public static boolean isNetworkAvailable(Context ctx) {
    ConnectivityManager cm =
        (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

    //NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
    NetworkInfo i = cm.getActiveNetworkInfo();
    if (i == null)
      return false;
    if (!i.isConnected())
      return false;
    if (!i.isAvailable())
      return false;
    return true;

  }

}
