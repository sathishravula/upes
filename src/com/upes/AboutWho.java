package com.upes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
 
public class AboutWho extends BaseActivity {
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.about_who, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("About Us");
        btnHeader.setVisibility(View.GONE);
        
		tvHeader.setText("Who We Are");
		
	}
	
}
