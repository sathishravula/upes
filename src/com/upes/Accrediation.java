package com.upes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.upes.object.AccrediationInfo;

public class Accrediation extends BaseActivity {

  LayoutInflater inflater;
  LinearLayout llMainContent;
  ListView lvAboutusDetail;
  ListAdapter listAdapter;

  AccrediationInfo accrediationInfo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    inflater = this.getLayoutInflater();
    llMainContent = new LinearLayout(this);
    llMainContent = (LinearLayout) inflater.inflate(R.layout.about_us_detail2, null);

    LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    llMain.addView(llMainContent, params);
    tvHeader.setText("Accrediations & Recognitions");
    btnHeader.setVisibility(View.GONE);

    lvAboutusDetail = (ListView) llMainContent.findViewById(R.id.lv_aboutus_detail);
    accrediationInfo = new AccrediationInfo();

    listAdapter = new ListAdapter();
    lvAboutusDetail.setAdapter(listAdapter);
    lvAboutusDetail.setSelector(android.R.color.transparent);

  }


  class ListAdapter extends BaseAdapter {
    @Override
    public int getCount() {
      // TODO Auto-generated method stub
      return accrediationInfo.tilte.length;
    }

    @Override
    public Object getItem(int position) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public long getItemId(int position) {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      convertView = inflater.inflate(R.layout.list_awards, null);

      TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title_about);
      TextView tvDesc = (TextView) convertView.findViewById(R.id.tv_desc_about);
      ImageView img = (ImageView) convertView.findViewById(R.id.imv_awards);

      tvTitle.setText(accrediationInfo.tilte[position]);
      //tvTitle.setTextColor(Color.BLACK);
      tvDesc.setText(accrediationInfo.description[position]);
      img.setImageResource(accrediationInfo.image[position]);
      return convertView;
    }

  }

}
