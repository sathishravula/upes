package com.upes;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.upes.object.AppsConstant;
import com.upes.object.AwardsInfo;

public class Awards extends BaseActivity {
	
	LayoutInflater inflater;
	LinearLayout llMainContent;
	ListView lvAwards;
	ListAdapter listAdapter;
	
	AwardsInfo awardsInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inflater=this.getLayoutInflater();
		llMainContent = new LinearLayout(this);
		llMainContent = (LinearLayout)inflater.inflate(R.layout.about_us_detail2, null);
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        llMain.addView(llMainContent, params);
        tvHeader.setText("Awards & Accolades");
        btnHeader.setVisibility(View.GONE);
        
        lvAwards = (ListView)llMainContent.findViewById(R.id.lv_aboutus_detail);

        awardsInfo = new AwardsInfo();
        
        listAdapter=new ListAdapter();
        lvAwards.setAdapter(listAdapter);
		
		lvAwards.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Awards.this, AwardsImage.class);
				intent.putExtra("pos", arg2);
				startActivity(intent);
				
			}
		});
        
	}
	
	
	class ListAdapter extends BaseAdapter
	{
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return awardsInfo.tilte.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			convertView = inflater.inflate(R.layout.list_awards, null);
			
			TextView tvTitle = (TextView)convertView.findViewById(R.id.tv_title_about);
			TextView tvDesc = (TextView)convertView.findViewById(R.id.tv_desc_about);
			ImageView img = (ImageView)convertView.findViewById(R.id.imv_awards);
			
			tvTitle.setText(awardsInfo.tilte[position]);
			tvDesc.setText(awardsInfo.description[position]);
			img.setImageResource(awardsInfo.image[position]);
			return convertView;
		}
		
	}
	
}