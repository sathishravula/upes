package com.codecube.facebook;

/*import com.facebook.android.DialogError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;*/

import android.util.Log;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

/**
 * Skeleton base class for RequestListeners, providing default error 
 * handling. Applications should handle these error conditions.
 *
 */
public abstract class BaseDialogListener implements Facebook.DialogListener {

    public void onFacebookError(FacebookError e) {
    	Log.d("base dialog",e.toString());
    	Log.e("is logout","true 11");
//		FlurryAgent.onError(Constants.FLURRY_ERROR_FACEBOOK, e.getMessage(), e.getClass().getName());
    }

    public void onError(DialogError e) {
    	Log.d("base dialog",e.toString());
    	Log.e("is logout","true 59648487");
//		FlurryAgent.onError(Constants.FLURRY_ERROR_NETWORK_OPERATION, e.getMessage(), e.getClass().getName());
    }

    public void onCancel() {        
    }
    
}
