/*
 *
O * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codecube.facebook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.codecube.facebook.SessionEvents.AuthListener;
import com.codecube.facebook.SessionEvents.LogoutListener;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.upes.ShareSocialText;
import com.upes.object.MyPreferences;

public class LoginButton extends ImageButton {

  private Facebook mFb;
  private Handler mHandler;
  private SessionListener mSessionListener = new SessionListener();
  private String[] mPermissions;
  private Activity mActivity;
  private MyPreferences myPreferences;

  public LoginButton(Context context) {
    super(context);
    myPreferences = new MyPreferences(context);
  }

  public LoginButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    myPreferences = new MyPreferences(context);
  }

  public LoginButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    myPreferences = new MyPreferences(context);
  }

  public void init(final Activity activity, final Facebook fb) {
    init(activity, fb, new String[]{});
//		myPreferences = new MyPreferences(activity.getApplicationContext());
  }

  public void init(final Activity activity, final Facebook fb, final String[] permissions) {
    mActivity = activity;
    mFb = fb;
    mPermissions = permissions;
    mHandler = new Handler();
    setBackgroundColor(Color.TRANSPARENT);
    setAdjustViewBounds(true);
    // setImageResource(fb.isSessionValid() ? R.drawable.logout_button :
    // R.drawable.login_button);
    drawableStateChanged();

    SessionEvents.addAuthListener(mSessionListener);
    SessionEvents.addLogoutListener(mSessionListener);
    setOnClickListener(new ButtonOnClickListener());
  }

  public final class ButtonOnClickListener implements OnClickListener {

    public void onClick(View arg0) {
      if (mFb.isSessionValid()) {
        Log.e("logout", "true");
        SessionEvents.onLogoutBegin();
        AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFb);
        asyncRunner.logout(getContext(), new LogoutRequestListener());
      } else {
        Log.e("logout", "false");
        mFb.authorize(mActivity, mPermissions, new LoginDialogListener());
      }
    }
  }

  public class LoginDialogListener implements DialogListener {

    public void onComplete(Bundle values) {
      SessionEvents.onLoginSuccess();
      saveCredentials(mFb);

      //			try {
      //				if (mActivity instanceof CreateDeal) {
      //					((CreateDeal) mActivity).postOnWall();
      //				} else if (mActivity instanceof DealDetailPage) {
      //					((DealDetailPage)mActivity).postOnWall();
      //				}
      //				else if (mActivity instanceof MoreOptionActivity) {
      //					((MoreOptionActivity)mActivity).postOnWall();
      //				}
      //				else {
      //					String response = mFb.request("/me");
      //					Log.e("*****facebook respnose***", "*************"
      //							+ response);
      //					// if(mActivity instanceof AppLoginActivity){
      //
      //					// calling facebook data
      //					facebookDataBean bean = parser.getfacebookInfo(response);
      //					new ServerMethods(mActivity).getSocialMediaLogin(
      //							AppConstants.EXISTING_USER,
      //							AppConstants.APPKEY_SERVER, bean.getSocialID(),
      //							"facebook", bean.getUsername(), bean.getEmailID());
      //					MyPreferences objpref = new MyPreferences(mActivity);
      //					objpref.setSocialType("facebook");
      //
      //				}
      //
      //			} catch (Exception e) {
      //
      //			}

      new ShareSocialText(mActivity).shareOnFacebookWall();
    }

    public void onFacebookError(FacebookError error) {
      SessionEvents.onLoginError(error.getMessage());
    }

    public void onError(DialogError error) {
      SessionEvents.onLoginError(error.getMessage());
    }

    public void onCancel() {
      SessionEvents.onLoginError("Action Canceled");
    }
  }

  public class SampleRequestListener extends BaseRequestListener {


    @Override
    public void onComplete(String response, Object state) {
      // TODO Auto-generated method stub

    }

    @Override
    public void onIOException(IOException e, Object state) {
      // TODO Auto-generated method stub

    }

    @Override
    public void onFileNotFoundException(FileNotFoundException e,
                                        Object state) {
      // TODO Auto-generated method stub

    }

    @Override
    public void onMalformedURLException(MalformedURLException e,
                                        Object state) {
      // TODO Auto-generated method stub

    }

    @Override
    public void onFacebookError(FacebookError e, Object state) {
      // TODO Auto-generated method stub

    }
  }

  public class LogoutRequestListener extends BaseRequestListener {

    @Override
    public void onComplete(String response, Object state) {
      mHandler.post(new Runnable() {
        public void run() {
          Log.e("is logout", "true");
          SessionEvents.onLogoutFinish();
        }
      });
    }

    @Override
    public void onIOException(IOException e, Object state) {

    }

    @Override
    public void onFileNotFoundException(FileNotFoundException e,
                                        Object state) {

    }

    @Override
    public void onMalformedURLException(MalformedURLException e, Object state) {

    }

    @Override
    public void onFacebookError(FacebookError e, Object state) {
      // TODO Auto-generated method stub

    }

//		@Override
//		public void onComplete(String arg0) {
//			mHandler.post(new Runnable() {
//				public void run() {
//					Log.e("is logout", "true");
//					SessionEvents.onLogoutFinish();
//				}
//			});
//		}
  }

  public class SessionListener implements AuthListener, LogoutListener {

    public void onAuthSucceed() {
    }

    public void onAuthFail(String error) {

    }

    public void onLogoutBegin() {

    }

    public void onLogoutFinish() {
      SessionStore.clear(getContext());
      // setImageResource(R.drawable.login_button);
    }
  }

  public void saveCredentials(Facebook facebook) {
    try {
//			save facebook access token here 
      myPreferences.setFacebookToken(facebook.getAccessToken());
      Log.e("Facebook Token saved in Preferences", "" + facebook.getAccessToken());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
