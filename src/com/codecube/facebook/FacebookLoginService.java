package com.codecube.facebook;

import twitter4j.User;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.facebook.android.Facebook;
import com.upes.object.AppsConstant;

public class FacebookLoginService 
{
	public static  String [] permissions;
	private static FacebookLoginService instance = null;
	private Facebook mFacebook;
	private Activity mActivity;
	private User user = null;

	private FacebookLoginService(Context context) {

	}

	public static FacebookLoginService getInstance(Context context) {
		if (instance == null ) {
			instance = new FacebookLoginService(context);
		}  
		return instance; 
	}

	public User getUser() {
		return user;
	}

	public void facebookLogin(Activity context, LoginButton mLoginButton) 
	{
		this.mActivity=context;
		mFacebook = AppsConstant.facebook; 
		SessionStore.restore(mFacebook, mActivity);
		mLoginButton.init(mActivity, mFacebook);
		permissions = new String[5];
		permissions[0] = "read_stream";  
		permissions[1] = "offline_access";
		permissions[2] = "publish_stream";
		permissions[3] = "email";
		permissions[4] = "user_birthday";
		if (!mFacebook.isSessionValid())
		{ 
			mFacebook.authorize(context, permissions,-1, mLoginButton.new LoginDialogListener());
		}
	}

	public void onActivityResultCallBack(int requestCode,int resultCode,Intent data)
	{
		mFacebook.authorizeCallback(requestCode, resultCode, data);  
	}
}
